<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('try', [
	'as' => 'try',
	'uses' => 'VVoucherController@trypage'
]);


Route::get('/', [
	'as' => 'dashboard',
	'uses' => 'DashboardController@index'
]);

Route::get('add-voucher', [
	'as' => 'add-voucher',
	'uses' => 'VVoucherController@create'
]);

Route::post('add-voucher', [
	'as' => 'add-voucher.post',
	'uses' => 'VVoucherController@store'
]);

// Route::post('subaccounthead', [
// 	'as' => 'subaccountheads.ajax.post',
// 	'uses' => 'VVoucherController@getSubAccountHead'
// ]);

Route::get('view-voucher', [
	'as' => 'view-all-voucher',
	'uses' => 'VVoucherController@index'
]);

Route::get('view/voucher/{id}', [
	'as' => 'view-voucher-single',
	'uses' => 'VVoucherController@show'
]);

Route::get('voucher/{id}/edit', [
    'as' => 'voucher.edit',
    'uses' => 'VVoucherController@edit'
]);

// Route::patch('voucher-update/{id}', [
//     'as' => 'voucher.update',
//     'uses' => 'VoucherController@update'
// ]);

// account head route
Route::get('add-account-head', [
	'as' => 'add-account-head',
	'uses' => 'AccountHeadController@create'
]);

Route::post('add-account-head', [
	'as' => 'add-account-head.post',
	'uses' => 'AccountHeadController@store'
]);

Route::get('view-account-head', [
	'as' => 'view-all-account-head',
	'uses' => 'AccountHeadController@index'
]);

Route::get('account-head/{id}/edit', [
    'as' => 'account-head.edit',
    'uses' => 'AccountHeadController@edit'
]);

Route::patch('account-head-update/{id}', [
    'as' => 'account-head.update',
    'uses' => 'AccountHeadController@update'
]);

Route::get('add-opening-balance', [
	'as' => 'add-opening-balance',
	'uses' => 'AccountHeadController@addOpeningBalance'
]);

Route::post('add-opening-balance', [
	'as' => 'add-opening-balance.post',
	'uses' => 'AccountHeadController@storeOpeningBalance'
]);

Route::post('get-accounthead', [
	'as' => 'getaccountheads.ajax.post',
	'uses' => 'AccountHeadController@getAccountHead'
]);
// end of account head route

// sub account head route
Route::get('add-sub-account-head', [
	'as' => 'add-sub-account-head',
	'uses' => 'SubAccountHeadController@create'
]);

Route::post('add-sub-account-head', [
	'as' => 'add-sub-account-head.post',
	'uses' => 'SubAccountHeadController@store'
]);

Route::get('view-sub-account-head', [
	'as' => 'view-sub-all-account-head',
	'uses' => 'SubAccountHeadController@index'
]);

Route::get('sub-account-head/{id}/edit', [
    'as' => 'sub-account-head.edit',
    'uses' => 'SubAccountHeadController@edit'
]);

Route::patch('sub-account-head-update/{id}', [
    'as' => 'sub-account-head.update',
    'uses' => 'SubAccountHeadController@update'
]);
// end of sub account head route


// cashbook/bankbook route
Route::post('view-cashbook', [
	'as' => 'view-cashbook.post',
	'uses' => 'CashbookController@viewFilterPage'
]);

Route::get('select-cashbook', [
	'as' => 'select-cashbook',
	'uses' => 'CashbookController@index'
]);

Route::get('add-cashbook', [
	'as' => 'add-cashbook',
	'uses' => 'CashbookController@addCashbook'
]);

Route::post('add-cashbook', [
	'as' => 'add-cashbook.post',
	'uses' => 'CashbookController@storeCashbook'
]);

// end of cashbook/bankbook route


// Head wise report route
Route::get('head-wise-report', [
	'as' => 'head-wise-report',
	'uses' => 'DashboardController@headWiseView'
]);

Route::get('head-wise-subhead/{id}', [
	'as' => 'head-wise-subhead',
	'uses' => 'DashboardController@headWiseSubhead'
]);

Route::get('subhead-details/{id}', [
	'as' => 'subhead-details',
	'uses' => 'DashboardController@headWiseSubheadDetails'
]);

Route::get('subhead-details-full/{id}', [
	'as' => 'subhead-details-full',
	'uses' => 'DashboardController@headWiseSubheadDetailsFull'
]);
// end of head wise report


// day wise report
Route::get('day-wise-report-filter', [
	'as' => 'day-wise-report-filter',
	'uses' => 'DashboardController@dayWiseViewFilter'
]);

Route::post('day-wise-report', [
	'as' => 'day-wise-report.post',
	'uses' => 'DashboardController@dayWiseView'
]);

Route::get('day-wise-report-full/{id}', [
	'as' => 'day-wise-report-full',
	'uses' => 'DashboardController@dayWiseSubheadDetailsFull'
]);

// end of day wise report


// receipts and payments wise report
Route::get('receipts-payments-report-filter', [
	'as' => 'receipts-payments-report-filter',
	'uses' => 'DashboardController@receiptPaymentWiseFilter'
]);

Route::post('receipts-payments-report', [
	'as' => 'receipts-payments-report.post',
	'uses' => 'DashboardController@receiptPaymentWise'
]);



