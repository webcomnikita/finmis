-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2018 at 02:13 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finmisc`
--

-- --------------------------------------------------------

--
-- Table structure for table `accountheads`
--

CREATE TABLE `accountheads` (
  `id` int(11) NOT NULL,
  `main_account_head_id` int(11) NOT NULL,
  `account_head` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `grant_parent_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accountheads`
--

INSERT INTO `accountheads` (`id`, `main_account_head_id`, `account_head`, `parent_id`, `grant_parent_id`, `created_at`, `updated_at`) VALUES
(1, 2, 'ADMINISTRATION', 88, 0, '2018-06-14 04:50:18', '2018-06-14 04:50:18'),
(2, 2, 'TRAVELLING ALLOWANCES', 88, 0, '2018-06-14 04:50:55', '2018-06-14 04:50:55'),
(3, 2, 'OFFICE CONTINGENCIES', 88, 0, '2018-06-14 04:51:51', '2018-06-14 04:51:51'),
(4, 2, 'ACADEMIC ACTIVITIES', 88, 0, '2018-06-14 10:39:40', '2018-06-14 04:52:04'),
(5, 2, 'MISC GENERAL EXPENDITURE', 88, 0, '2018-06-14 04:52:49', '2018-06-14 04:52:49'),
(6, 2, 'EXAMINATION', 88, 0, '2018-06-14 04:53:03', '2018-06-14 04:53:03'),
(7, 2, 'CONFIDENTIAL EXPENSES', 88, 0, '2018-06-14 04:53:19', '2018-06-14 04:53:19'),
(8, 2, 'EXAMINATION CONTINGENCIES', 88, 0, '2018-06-14 04:53:33', '2018-06-14 04:53:33'),
(9, 1, 'REVENUE RECEIPTS', 87, 0, '2018-06-14 04:53:53', '2018-06-14 04:53:53'),
(10, 1, 'CAPITAL RECEIPT', 87, 0, '2018-06-14 04:54:07', '2018-06-14 04:54:07'),
(11, 1, 'MISC RECEIPT', 87, 0, '2018-06-14 04:54:19', '2018-06-14 04:54:19'),
(12, 1, 'ACADEMIC FEES RECEIPT', 87, 0, '2018-06-14 04:54:34', '2018-06-14 04:54:34'),
(13, 1, 'DEBTS AND DEPOSITS', 87, 0, '2018-06-14 04:54:46', '2018-06-14 04:54:46'),
(14, 1, 'DRAFTS/CHALLANS COLLECTED BUT NOT DEPOSITED INTO BANK', 87, 0, '2018-06-14 04:55:55', '2018-06-14 04:55:55'),
(15, 3, 'APEX BANK OF INDIA (NOONMATI) 883/13', 89, 0, '2018-06-14 04:56:20', '2018-06-14 04:56:20'),
(16, 3, 'APEX BANK OF INDIA (NOONMATI) 618/3', 89, 0, '2018-06-14 04:56:34', '2018-06-14 04:56:34'),
(17, 3, 'APEX BANK OF INDIA (SILPUKHRI) 03084/24', 89, 0, '2018-06-14 04:56:46', '2018-06-14 04:56:46'),
(18, 4, 'KALITA STORES', 90, 0, '2018-06-14 04:56:57', '2018-06-14 04:56:57'),
(19, 4, 'JAYA STORES', 90, 0, '2018-06-14 04:57:08', '2018-06-14 04:57:08');

-- --------------------------------------------------------

--
-- Table structure for table `accountopeningbalances`
--

CREATE TABLE `accountopeningbalances` (
  `id` int(11) NOT NULL,
  `account_head_id` int(11) NOT NULL,
  `voucher_no` int(11) NOT NULL,
  `opening_balance` double NOT NULL,
  `opening_balance_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accountopeningbalances`
--

INSERT INTO `accountopeningbalances` (`id`, `account_head_id`, `voucher_no`, `opening_balance`, `opening_balance_date`, `created_at`, `updated_at`) VALUES
(1, 12, 6, 5000, '2018-02-07', '2018-05-25 05:25:48', '0000-00-00 00:00:00'),
(2, 13, 3, 3000, '2018-01-12', '2018-05-25 05:25:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `accounttransactions`
--

CREATE TABLE `accounttransactions` (
  `id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `voucher_no` varchar(255) DEFAULT NULL,
  `generated_voucher_no` varchar(255) DEFAULT NULL,
  `voucher_date` date NOT NULL,
  `voucher_account_type` varchar(100) DEFAULT NULL,
  `debit_id` int(11) NOT NULL,
  `debit_name` varchar(255) DEFAULT NULL,
  `credit_id` int(11) DEFAULT NULL,
  `credit_name` varchar(255) DEFAULT NULL,
  `v_amount` bigint(20) NOT NULL,
  `trans_type` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounttransactions`
--

INSERT INTO `accounttransactions` (`id`, `voucher_id`, `voucher_no`, `generated_voucher_no`, `voucher_date`, `voucher_account_type`, `debit_id`, `debit_name`, `credit_id`, `credit_name`, `v_amount`, `trans_type`, `created_at`, `updated_at`) VALUES
(82, 5, '999', 'FINMIS/2018-19/F/999', '2018-04-01', 'receipt', 1, 'ADMINISTRATION', 2, 'Pass Certificate Fees', 3000, 'debit', '2018-06-20 03:15:34', '2018-06-20 03:15:34'),
(83, 5, '999', 'FINMIS/2018-19/F/999', '2018-04-01', 'receipt', 6, 'EXAMINATION', 4, 'Re- Checking Fees', 3000, 'credit', '2018-06-20 03:15:34', '2018-06-20 03:15:34'),
(84, 6, '67657', 'FINMIS/2018-19/F/67657', '2018-05-07', 'receipt', 6, 'EXAMINATION', 11, 'Interest from Security', 3000, 'debit', '2018-06-20 03:17:08', '2018-06-20 03:17:08'),
(85, 6, '67657', 'FINMIS/2018-19/F/67657', '2018-05-07', 'receipt', 18, 'KALITA STORES', 17, 'Enrolment Fees', 3000, 'credit', '2018-06-20 03:17:08', '2018-06-20 03:17:08'),
(86, 7, '67657', 'FINMIS/2018-19/F/67657', '2018-05-07', 'receipt', 6, 'EXAMINATION', 11, 'Interest from Security', 3000, 'debit', '2018-06-20 03:17:53', '2018-06-20 03:17:53'),
(87, 7, '67657', 'FINMIS/2018-19/F/67657', '2018-05-07', 'receipt', 18, 'KALITA STORES', 17, 'Enrolment Fees', 3000, 'credit', '2018-06-20 03:17:53', '2018-06-20 03:17:53'),
(88, 8, '67657', 'FINMIS/2018-19/F/67657', '2018-05-07', 'receipt', 6, 'EXAMINATION', 11, 'Interest from Security', 3000, 'debit', '2018-06-20 03:18:17', '2018-06-20 03:18:17'),
(89, 8, '67657', 'FINMIS/2018-19/F/67657', '2018-05-07', 'receipt', 18, 'KALITA STORES', 17, 'Enrolment Fees', 3000, 'credit', '2018-06-20 03:18:17', '2018-06-20 03:18:17');

-- --------------------------------------------------------

--
-- Table structure for table `accounttransmasters`
--

CREATE TABLE `accounttransmasters` (
  `id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `entry_no` int(11) NOT NULL,
  `voucher_narration` text NOT NULL,
  `voucher_amount` bigint(20) NOT NULL,
  `entered_by` varchar(255) NOT NULL,
  `entered_on` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounttransmasters`
--

INSERT INTO `accounttransmasters` (`id`, `voucher_id`, `entry_no`, `voucher_narration`, `voucher_amount`, `entered_by`, `entered_on`, `created_at`, `updated_at`) VALUES
(45, 5, 999, 'Being amount receipt from  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic ipsam dolor molestiae reiciendis quae perspiciatis officia, ut, vel blanditiis id!', 3000, 'dd', '2018-06-20', '2018-06-20 03:15:34', '2018-06-20 03:15:34'),
(46, 8, 67657, 'Being amount receipt from  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic ipsam dolor molestiae reiciendis quae perspiciatis officia, ut, vel blanditiis id!', 3000, 'challan', '2018-06-20', '2018-06-20 03:18:17', '2018-06-20 03:18:17');

-- --------------------------------------------------------

--
-- Table structure for table `bankmasters`
--

CREATE TABLE `bankmasters` (
  `id` int(11) NOT NULL,
  `bankname` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bankmasters`
--

INSERT INTO `bankmasters` (`id`, `bankname`, `created_at`, `updated_at`) VALUES
(1, 'The Assam Co Op Apex Bank Ltd. (Guwahati Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(2, 'The Assam Co Op Apex Bank Ltd. (Bamunimaidan Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(3, 'The Assam Co Op Apex Bank Ltd. (Noonmati Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(4, 'The Assam Co Op Apex Bank Ltd. (Silpukhri Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(5, 'The Assam Co Op Apex Bank Ltd. (Ganeshguri Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(6, 'State Bank Of India (Guwahati Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(7, 'State Bank Of India (New Guwahati Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(8, 'State Bank Of India (CPI Division Guwahati)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(9, 'State Bank Of India (Silpukhri Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(10, 'State Bank Of India (Maligaon Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(11, 'State Bank Of India (Dispur Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(12, 'State Bank Of India (Ganeshguri Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(13, 'State Bank Of India (South Guwahati Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(14, 'State Bank Of India (Narengi Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(15, 'State Bank Of India (GMC Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(16, 'State Bank Of India (Guwahati Refinery Complex Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(17, 'State Bank Of India (West Guwahati Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(18, 'State Bank Of India (Pandu Port Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(19, 'State Bank Of India (Chenikuthi Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(20, 'State Bank Of India (Guwahati University Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(21, 'State Bank Of India (Kalipur Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(23, 'State Bank Of India (Guwahati Airport Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(24, 'State Bank Of India (GeetaNagar Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(25, 'State Bank Of India (Modghuria Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(26, 'State Bank Of India (VinovaNagar Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(27, 'State Bank Of India (Fancy Bazar Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(28, 'United Bank Of India (Guwahati Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(29, 'United Bank Of India (Noonmati Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00'),
(30, 'United Bank Of India (AT Road Branch)', '2018-04-26 12:16:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cashbooks`
--

CREATE TABLE `cashbooks` (
  `id` bigint(20) NOT NULL,
  `cbno` bigint(20) NOT NULL,
  `opneningbalance` double NOT NULL,
  `closingbalance` double NOT NULL,
  `account_head_id` int(11) NOT NULL,
  `sub_head_id` int(11) NOT NULL,
  `cashbookpostingdate` date NOT NULL,
  `accoundheadtype` varchar(50) NOT NULL,
  `vaouchertype` varchar(50) NOT NULL,
  `receiptno` varchar(50) NOT NULL,
  `amount` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `challans`
--

CREATE TABLE `challans` (
  `id` int(11) NOT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `voucher_no` varchar(100) DEFAULT NULL,
  `generated_voucher_no` varchar(255) DEFAULT NULL,
  `challan_no` varchar(50) DEFAULT NULL,
  `challan_date` date DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `deposit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 (Not Deposited for Collection), 1 (Deposited for Collection)',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `challans`
--

INSERT INTO `challans` (`id`, `voucher_id`, `voucher_no`, `generated_voucher_no`, `challan_no`, `challan_date`, `bank_id`, `bank_name`, `deposit`, `created_at`, `updated_at`) VALUES
(3, 8, '67657', 'FINMIS/2018-19/F/67657', 'fv5t5tttttttty', '2018-05-14', 15, 'State Bank Of India (GMC Branch)', 0, '2018-06-20 03:18:17', '2018-06-20 03:18:17');

-- --------------------------------------------------------

--
-- Table structure for table `ddrafts`
--

CREATE TABLE `ddrafts` (
  `id` int(11) NOT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `voucher_no` varchar(100) DEFAULT NULL,
  `generated_voucher_no` varchar(255) DEFAULT NULL,
  `dd_no` varchar(50) DEFAULT NULL,
  `dd_date` date DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `deposit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 (Not Deposited for Collection), 1 (Deposited for Collection)',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ddrafts`
--

INSERT INTO `ddrafts` (`id`, `voucher_id`, `voucher_no`, `generated_voucher_no`, `dd_no`, `dd_date`, `bank_id`, `bank_name`, `deposit`, `created_at`, `updated_at`) VALUES
(5, 5, '999', 'FINMIS/2018-19/F/999', 'htyhy7678', '2018-05-02', 1, 'The Assam Co Op Apex Bank Ltd. (Guwahati Branch)', 0, '2018-06-20 03:15:34', '2018-06-20 03:15:34');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(9) NOT NULL,
  `emp_qualification_id` int(11) DEFAULT NULL,
  `emp_f_name` varchar(60) DEFAULT NULL,
  `emp_m_name` varchar(60) DEFAULT NULL,
  `emp_l_name` varchar(60) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `fld_DeptID` int(11) DEFAULT NULL,
  `emp_dob` varchar(60) DEFAULT NULL,
  `emp_date_of_joining` varchar(60) DEFAULT NULL,
  `emp_gender` varchar(7) DEFAULT NULL,
  `service_image` varchar(200) DEFAULT NULL,
  `signature` varchar(200) DEFAULT NULL,
  `emp_type` varchar(40) DEFAULT NULL,
  `probation_or_not` varchar(20) DEFAULT NULL,
  `emp_blood_group` varchar(30) DEFAULT NULL,
  `emp_contact_no` varchar(15) DEFAULT NULL,
  `emp_alt_contact_no` varchar(15) DEFAULT NULL,
  `pan_no` varchar(255) NOT NULL,
  `pran_no` varchar(255) NOT NULL,
  `bank_account_number` varchar(127) DEFAULT NULL,
  `bank_name` varchar(120) DEFAULT NULL,
  `ifsc` varchar(100) DEFAULT NULL,
  `pension_bank_account_no` varchar(60) DEFAULT NULL,
  `emp_present_house_no` varchar(15) DEFAULT NULL,
  `emp_present_locality` varchar(60) DEFAULT NULL,
  `emp_present_city` varchar(60) DEFAULT NULL,
  `emp_present_street` varchar(100) DEFAULT NULL,
  `emp_present_pin` varchar(40) DEFAULT NULL,
  `emp_present_district` varchar(60) DEFAULT NULL,
  `emp_permanent_house_no` varchar(20) DEFAULT NULL,
  `emp_permanent_locality` varchar(60) DEFAULT NULL,
  `emp_permanent_city` varchar(60) DEFAULT NULL,
  `emp_permanent_street` varchar(120) DEFAULT NULL,
  `emp_permanent_pin` varchar(40) DEFAULT NULL,
  `emp_permanent_district` varchar(60) DEFAULT NULL,
  `emp_experience` varchar(200) DEFAULT NULL,
  `emp_cast` varchar(8) DEFAULT NULL,
  `emp_religion` varchar(20) DEFAULT NULL,
  `emp_date_of_regularization` date DEFAULT NULL,
  `emp_date_of_death` date DEFAULT NULL,
  `emp_date_of_retirement` date DEFAULT NULL,
  `submission_date` date DEFAULT NULL,
  `casual_pay` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `priority` int(11) NOT NULL DEFAULT '999',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `emp_qualification_id`, `emp_f_name`, `emp_m_name`, `emp_l_name`, `post_id`, `fld_DeptID`, `emp_dob`, `emp_date_of_joining`, `emp_gender`, `service_image`, `signature`, `emp_type`, `probation_or_not`, `emp_blood_group`, `emp_contact_no`, `emp_alt_contact_no`, `pan_no`, `pran_no`, `bank_account_number`, `bank_name`, `ifsc`, `pension_bank_account_no`, `emp_present_house_no`, `emp_present_locality`, `emp_present_city`, `emp_present_street`, `emp_present_pin`, `emp_present_district`, `emp_permanent_house_no`, `emp_permanent_locality`, `emp_permanent_city`, `emp_permanent_street`, `emp_permanent_pin`, `emp_permanent_district`, `emp_experience`, `emp_cast`, `emp_religion`, `emp_date_of_regularization`, `emp_date_of_death`, `emp_date_of_retirement`, `submission_date`, `casual_pay`, `status`, `priority`, `created_at`, `updated_at`) VALUES
(1003, 2, 'MD MUYIZUR', '', 'RAHMAN', 10, 12, '1960-03-04', '', 'Male', NULL, NULL, 'Permanent', 'No', 'O -ve', '0', '1111111111', '', '', '13158', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2020-03-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1006, 2, 'NATHU ', 'RAM', 'BORO', 10, 16, '1957-05-01', NULL, 'Male', NULL, NULL, 'Permanent', 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-05-31', NULL, 0, 0, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1008, 2, 'DIGANTA', '', 'SARMAH', 10, 12, '1961-03-01', '', 'MALE', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '13152', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', 'HINDU', NULL, NULL, '2021-03-31', '2017-06-22', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1009, 2, 'MRIDUL', 'KR', 'BARUAH', 11, 12, '1961-09-25', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '12047', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2021-09-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1010, 2, 'BHUBAN', 'KR', 'DAS', 10, 12, '1962-12-31', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '10928', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2022-12-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1011, 2, 'JITENDRA', 'NATH', 'TALUKDAR', 11, 12, '1960-09-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '11243', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2020-09-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1014, 2, 'ANANDA', 'KR', 'SARMA', 10, 14, '1959-12-31', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '14421', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2019-12-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1015, 2, 'PRAFULLA', '', 'BAYAN', 10, 12, '1961-01-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '14423', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2021-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1017, 2, 'PABIN', 'CH', 'DAS', 10, 12, '1962-01-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '10521', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2022-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1018, 2, 'KAMALESWAR', '', 'SARMA', 10, 12, '1963-02-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '12934', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2023-02-28', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1019, 2, 'BHUPENDRA', 'NATH', 'DAS', 11, 12, '1963-07-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '13420', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2023-07-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1020, 2, 'BABUL', 'CH', 'DAS', 10, 12, '1963-11-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '13306', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2023-11-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1021, 2, 'KAMESWAR', '', 'KALITA', 7, 12, '1964-09-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '13492', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2024-09-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1022, 2, 'BIJAN', '', 'DAS', 10, 12, '1964-10-24', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '14424', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2024-10-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1024, 2, 'DINESH', '', 'RAJBONGSHI', 16, 12, '1965-03-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '12846', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2025-03-31', '2018-02-10', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1025, 2, 'MADAN', 'CH', 'KALITA', 16, 12, '1965-05-12', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '13295', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2025-05-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1026, 2, 'GOKUL', '', 'SARMA', 11, 12, '1966-01-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '13552', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2026-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1027, 2, 'BIRAJ', 'KRISHNA', 'DAS', 11, 12, '1966-02-23', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '12686', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2026-02-28', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1028, 2, 'JYOTISH', '', 'HALOI', 11, 12, '1966-08-04', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '11035', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2026-08-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1029, 2, 'BIBHUTI', 'KR', 'DAS', 11, 12, '1966-09-04', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '11352', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2026-09-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1030, 2, 'DINESH', 'CH', 'BEY', 15, 12, '1967-01-06', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '114405', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2027-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1031, 2, 'ISLAMUDDIN', '', 'AHMED', 16, 12, '1967-03-23', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '11233', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2027-03-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1032, 2, 'MD MAJNUR', '', 'ALI', 16, 12, '1967-04-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '11945', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2027-04-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1033, 2, 'KALIN', 'CH', 'DEKA', 15, 12, '1967-10-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '13244', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2027-10-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1034, 2, 'KANAK', 'CH', 'BORAH', 16, 12, '1968-02-29', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '11253', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2028-02-29', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1035, 2, 'SUREN ', 'CH', 'DAS', 16, 12, '1968-10-18', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '11320', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2028-10-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1036, 2, 'BHUPEN', '', 'KALITA', 16, 12, '1968-11-30', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '11975', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2028-11-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1037, 2, 'PRADIP', 'KR', 'BAISHYA', 11, 16, '1969-01-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '12819', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2029-01-31', '2017-07-10', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1038, 2, 'NARAYAN', 'CH', 'DAS', 16, 12, '1969-03-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '11768', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2029-03-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1039, 2, 'SARU', 'BALA', 'RAHANG', 15, 12, '1970-01-01', '', 'Female', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '13538', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2030-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1040, 2, 'BHUPEN', '', 'DOLEY', 15, 12, '1970-03-05', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '14214', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2030-03-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1041, 2, 'PRADIP', 'KR', 'DAS', 15, 12, '1970-07-29', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '13559', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2030-07-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1043, 2, 'DIPAK', '', 'BHATTA', 16, 12, '1971-05-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '11253', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2031-05-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1044, 2, 'MRIDUL', 'KR', 'SARMA', 11, 12, '1974-10-19', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '11921', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2034-10-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1045, 2, 'AJIT', '', 'DEOGHARIA', 15, 12, '1977-01-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '13645', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2037-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1046, 2, 'BHASKAR JYOTI', '', 'KALITA', 16, 12, '1977-03-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '11268', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2037-03-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1047, 2, 'RUBUL', 'KR', 'BRAHMA', 15, 12, '1983-03-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '', '', '', '', '13588', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2043-03-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1052, 2, 'BIRENDRA', 'NATH', 'DEKA', 14, 12, '1966-06-30', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11236', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2026-06-30', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1053, 2, 'DIPANJALI', '', 'DAS', 13, 12, '1973-04-23', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '13335', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2033-04-30', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1054, 2, 'ABDUL', '', 'HAQUE', 16, 12, '1970-01-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11355', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2030-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1055, 2, 'SATYEN', '', 'BHARALI', 16, 12, '1972-03-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '12981', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2032-03-31', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1056, 2, 'TARUN', '', 'KALITA', 19, 1, '1967-08-31', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11270', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2027-08-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1057, 2, 'BHUPESH', '', 'KALITA', 19, 1, '1968-01-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11773', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2028-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1058, 2, 'KHITISH', 'CH', 'DAS', 19, 1, '1967-04-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11916', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2027-04-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1059, 2, 'JITEN', '', 'SONOWAL', 20, 12, '1958-06-30', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11246', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2018-06-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1060, 2, 'MANINDRA', '', 'DAS', 20, 12, '1967-03-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '13127', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2027-03-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1061, 2, 'BIJOY', 'Kr', 'NATH', 21, 12, '1965-01-31', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11025', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2025-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1062, 2, 'NRIPEN', '', 'DAS', 21, 12, '1966-12-31', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11244', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2026-12-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1063, 2, 'Md AZIZUR', '', 'RAHMAN', 21, 12, '1966-12-31', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11937', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2026-12-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1064, 2, 'GIRISH', 'CH', 'KALITA', 21, 12, '1967-02-28', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11685', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2027-02-28', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1065, 2, 'NABIN', '', 'BARUAH', 21, 12, '1961-03-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11084', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2021-03-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1066, 2, 'MRINAL', '', 'BARMAN', 21, 12, '1963-01-31', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11950', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2023-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1067, 2, 'DAMBARU', '', 'DAS', 21, 12, '1966-01-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11914', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2026-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1068, 2, 'NRIPEN', '', 'BARMAN', 21, 12, '1971-01-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11354', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2031-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1069, 2, 'NABA', '', 'SARMA', 21, 12, '1963-04-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '14406', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2023-04-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1070, 2, 'PHATIK', 'CH', 'KALITA', 21, 12, '1965-05-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11910', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2025-05-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1071, 2, 'HIREN', '', 'DAS', 21, 12, '1970-02-02', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11323', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2030-02-28', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1072, 2, 'MIRA', '', 'CHOUDHURY', 21, 12, '1973-04-17', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '12827', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2033-04-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1073, 2, 'SIDDHESWAR', '', 'KALITA', 21, 12, '1965-06-30', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '9384', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2025-06-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1074, 2, 'SUNIL', '', 'ROY', 21, 12, '1971-04-24', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11919', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2031-04-30', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1075, 2, 'KAILASH', '', 'KALITA', 21, 12, '1974-01-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11237', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2034-01-31', '2017-07-27', 0, 5, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1076, 2, 'DANI', 'RAM', 'KALITA', 21, 12, '1978-02-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11319', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2038-02-28', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1077, 2, 'GANESH', '', 'BORO', 21, 12, '1978-05-16', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '12974', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2038-05-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1079, 2, 'BINAY', '', 'TALUKDAR', 16, 12, '1973-03-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '12474', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2033-03-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1080, 2, 'RAHIM', '', 'ALI', 21, 12, '1973-06-30', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '12420', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2033-06-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1081, 2, 'AMAR', '', 'SARMA', 17, 12, '1969-10-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '13463', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2029-10-31', '2017-06-22', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1082, 2, 'MUKUNDA', '', 'DAS', 21, 12, '1975-03-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '13094', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2035-03-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1083, 2, 'ANUP', '', 'DAS', 21, 12, '1972-03-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '13450', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2032-03-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1084, 2, 'SUKLESWAR', '', 'DAS', 21, 12, '1964-02-19', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11955', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2024-02-29', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1085, 2, 'SURENDRA', 'NATH', 'DEKA', 21, 12, '1964-12-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11263', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2024-12-31', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1086, 2, 'Md JALIL', '', 'ALI', 21, 12, '1966-12-31', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '14420', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2026-12-31', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1087, 2, 'JITEN', '', 'KALITA', 21, 12, '1976-03-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '14982', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2036-03-31', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1088, 2, 'PRABHAT', '', 'SHARMA', 21, 12, '1973-08-12', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '14967', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2033-08-31', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1089, 2, 'HEMEN', '', 'DEKA', 21, 12, '1970-01-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '14983', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2030-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1090, 2, 'DIMBESWAR', '', 'CHOUDHURY', 21, 12, '1979-01-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '12553', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2039-01-31', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1091, 2, 'DIPAK', '', 'DAS', 21, 12, '1976-02-23', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '14978', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2036-02-29', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1092, 2, 'AJAY', '', 'MUDIAR', 21, 12, '1972-07-07', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '14727', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2032-07-31', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1093, 2, 'MUNIN', '', 'DEKA', 21, 12, '1977-01-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '14999', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2037-01-31', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1094, 2, 'SABERUDDIN', '', 'AHMED', 21, 12, '1979-01-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '14836', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2039-01-31', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1095, 2, 'PABAN', '', 'TALUKDAR', 21, 12, '1976-07-13', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '14975', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2036-07-31', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1096, 2, 'DILIP', '', 'SARMA', 21, 12, '1978-02-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '14977', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2038-02-28', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1098, 2, 'JOGEN', NULL, 'LAHKAR', 21, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(1099, 2, 'JANAK', NULL, 'NATH', 21, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2001, 2, 'PRAHLAD', NULL, 'DAS', 16, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2002, 2, 'UPEN', NULL, 'KALITA', 16, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2003, 2, 'ANJAN', NULL, 'SARMA', 16, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2004, 2, 'SIBCHARAN', NULL, 'KALITA', 16, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2005, 2, 'PANKAJ', '', 'CHOUDHURY', 19, 12, '1987-07-15', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '14990', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2047-07-31', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2006, 2, 'ABDUL', '', 'KALAM', 19, 12, '1965-04-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '15307', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2025-04-30', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2007, 2, 'KHARGESWAR', NULL, 'NATH', 21, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2008, 2, 'NIRANJAN', NULL, 'MEDHI', 21, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2009, 2, 'RAMESH', NULL, 'KALITA', 21, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2010, 2, 'RANJIT', NULL, 'RABHA', 21, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2011, 2, 'RAJU', NULL, 'PATOWARI', 21, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2012, 2, 'ANIL', NULL, 'BARMAN', 21, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2013, 2, 'DULU', NULL, 'DAS', 21, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2014, 2, 'SANTI', NULL, 'BASFOR', 37, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2015, 2, 'SARDA', NULL, 'BASFOR', 37, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2016, 2, 'MANJU', NULL, 'BASFOR', 37, 12, NULL, NULL, NULL, NULL, NULL, NULL, 'No', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2017, 11, 'DIPEN', 'KISHOR', 'LUKHURAKHAN', 4, 16, '1970-01-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '10698290360', 'SBI', 'SBIN0001244', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2030-01-31', '2018-04-03', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2018, 12, 'Dr DHARANI', 'DHAR', 'GOSWAMI', 4, 3, '1959-12-31', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '1282', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2019-12-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2019, 2, 'ARPAN', 'Kr', 'BARMAN', 5, 12, '1969-01-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '12108', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2029-01-31', '2017-06-19', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2020, 2, 'BUBUL', '', 'KAKOTI', 7, 16, '1962-06-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11917', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2022-06-30', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2021, 12, 'INDRANI', '', 'DAS', 4, 11, '1962-10-25', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '14592', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2022-10-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2023, 2, 'DHANESHWAR', '', 'DAS', 7, 12, '1959-01-01', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11932', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2019-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2024, 2, 'NARENDRA', '', 'GOSWAMI', 7, 3, '1960-12-04', '', '', NULL, NULL, 'Permanent', 'No', '', '', '', '', '', '11353', 'Apex Bank', 'HDFCOCACABL', NULL, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, '', '', NULL, NULL, '2020-12-31', '2017-06-17', 0, 0, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2026, 15, 'PHUL', 'KONWAR', 'PUJARI', 9, 16, '1972-12-19', '', 'Male', NULL, NULL, 'Permanent', 'No', 'A+', '999999', '', '', '', '15404', 'Apex Bank', 'HDFCOCACABL', NULL, 'NA', 'Na', 'Na', 'Na', '999999', 'Na', 'NA', 'Na', 'Na', 'Na', '999999', 'Na', '', '', '', NULL, NULL, '2032-12-31', '2017-07-27', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2027, 13, 'RANJAN', 'Kr', 'DAS', 4, 3, '1967-02-01', '', 'Male', NULL, NULL, 'Permanent', 'No', 'A+', '9999999', '', '', '', '12541', 'Apex Bank', 'HDFCOCACABL', NULL, 'NA', 'NA', 'NA', '', '999999', 'NA', 'NA', 'NA', 'NA', '', '999999', 'NA', '', '', '', NULL, NULL, '2027-02-28', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2028, 3, 'BAPAN', 'Ch', 'BARMAN', 7, 6, '1963-02-01', '', 'Male', NULL, NULL, 'Permanent', 'No', 'A+', '9999999', '', '', '', '13470', 'Apex Bank', 'HDFCOCACABL', NULL, 'NA', 'NA', 'NA', 'NA', '999999', 'NA', 'NA', 'NA', 'NA', 'NA', '999999', 'NA', '', '', '', NULL, NULL, '2023-02-28', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2029, 2, 'DURGA', 'Pd', 'SAHARIA', 7, 1, '1962-08-01', '', 'Male', NULL, NULL, 'Permanent', 'No', 'A+', '9999999', '', '', '', '11659', 'Apex Bank', 'HDFCOCACABL', NULL, 'NA', 'Na', 'NA', 'NA', '999999', 'Na', 'NA', 'Na', 'NA', 'NA', '999999', 'Na', '', '', '', NULL, NULL, '2022-08-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2031, 4, 'JYOTI', '', 'KALITA', 4, 1, '1970-01-01', '', 'Male', NULL, NULL, 'Permanent', 'No', '0', '9999999999', '', '', '', '14363', 'Assam Co Op Apex Bank', 'HDFC0CACABL', NULL, 'NA', 'NA', 'NA', 'NA', '999999', 'NA', 'NA', 'NA', 'NA', 'NA', '999999', 'NA', '', '', '', NULL, NULL, '2030-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2032, 14, 'NABAJYOTI', '', 'BHARALI', 6, 1, '1985-03-21', '', 'Male', NULL, NULL, 'Permanent', 'No', 'A+', '9999999999', '', '', '', '15186', 'Apex Bank', 'HDFC0CACABL', NULL, 'NA', 'NA', 'NA', 'NA', '999999', 'NA', 'NA', 'NA', 'NA', 'NA', '999999', 'NA', '', '', '', NULL, NULL, '2045-03-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2033, 4, 'PRAFULLA', '', 'TALUKDAR', 8, 1, '1964-04-06', '', 'Male', NULL, NULL, 'Permanent', 'No', 'A+', '9999999999', '', '', '', '11265', 'Apex Bank', 'HDFC0CACABL', NULL, 'NA', 'NA', 'NA', 'NA', '999999', 'NA', 'NA', 'NA', 'NA', 'NA', '999999', 'NA', '', '', '', NULL, NULL, '2024-04-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2035, 2, 'PRADIP', 'KR', 'KAKATI', 7, 3, '1958-09-01', '', 'Male', NULL, NULL, 'Permanent', 'No', 'A+', '9999999999', '', '', '', '12734', 'Apex Bank', 'HDFCOCACABL', NULL, 'NA', 'NA', 'NA', 'NA', '999999', 'NA', 'NA', 'NA', 'NA', 'NA', '999999', 'NA', '', '', '', NULL, NULL, '2018-09-30', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2036, 2, 'ANURUPA', '', 'CHOUDHURY', 4, 16, '1970-01-01', '', 'Female', NULL, NULL, 'Permanent', 'No', 'A+', '9999999999', '', '', '', '15441', 'Assam Co Op Apex Bank', 'HDFCOCACABL', NULL, 'NA', 'NA', 'NA', 'NA', '999999', 'NA', 'NA', 'NA', 'NA', 'NA', '999999', 'NA', '', '', '', NULL, NULL, '2030-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2037, 2, 'ABANI', '', 'TALUKDAR', 7, 11, '1962-01-01', '', 'Male', NULL, NULL, 'Permanent', 'No', 'A+', '9999999999', '', '', '', '11918', 'Apex Bank', 'HDFCOCACABL', NULL, 'NA', 'NA', 'NA', 'NA', '999999', 'NA', 'NA', 'NA', 'NA', 'NA', '999999', 'NA', '', '', '', NULL, NULL, '2022-01-31', '2017-06-17', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2038, 20, 'Amulya', 'Kr', 'Mazumdar', 10, 12, '1965-01-01', '', 'Male', NULL, NULL, 'Permanent', 'No', 'B-', '0000000000', '', '', '', '11309', 'Apex Bank', 'HDFCOCACABL', NULL, '0', 'NA', 'NA', 'NA', '000000', 'NA', '0', 'NA', 'NA', 'NA', '000000', 'NA', '', '', '', NULL, NULL, '2025-01-31', '2017-06-29', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2039, 18, 'Bipul', 'Kr', 'Das', 21, 1, '1970/01/01', '1970/01/01', 'Male', NULL, NULL, 'Permanent', 'No', '0', '9999999999', '', '', '', '15231', 'Apex Bank', 'HDFCOCACABL', NULL, 'o', 'NA', 'NA', 'NA', '000000', 'NA', 'o', 'NA', 'NA', 'NA', '000000', 'NA', '', '', '', NULL, NULL, '2030-01-31', '2017-06-19', 0, 1, 0, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2040, 20, 'Himangshu', '', 'Deka', 21, 1, '1990/02/09', '2017/08/09', 'Male', NULL, NULL, 'Permanent', 'Yes', 'A+', '9999999999', '', '', '', '999999', 'ABC', '999999', NULL, '99', 'Yuva Nagar', 'Guwahati', 'Forest Gate', '781006', 'KamrupM', '99', 'Yuva Nagar', 'Guwahati', 'Forest Gate', '781006', 'KamrupM', '', '', '', NULL, NULL, '2050-02-28', '2017-08-16', 0, 1, 999, '2018-04-26 12:21:35', '0000-00-00 00:00:00'),
(2041, 2, 'Proloy', NULL, 'Roy', 3, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 999, '2018-04-26 12:21:35', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mainaccountheads`
--

CREATE TABLE `mainaccountheads` (
  `id` int(11) NOT NULL,
  `code_id` int(11) NOT NULL,
  `major_account_head` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mainaccountheads`
--

INSERT INTO `mainaccountheads` (`id`, `code_id`, `major_account_head`, `created_at`, `updated_at`) VALUES
(1, 87, 'RECEIPTS', '2018-04-27 05:09:55', '0000-00-00 00:00:00'),
(2, 88, 'PAYMENTS', '2018-04-27 05:09:55', '0000-00-00 00:00:00'),
(3, 89, 'BANK ACCOUNTS', '2018-04-27 05:09:55', '0000-00-00 00:00:00'),
(4, 90, 'GENERAL', '2018-04-27 05:09:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `openingbalances`
--

CREATE TABLE `openingbalances` (
  `id` int(11) NOT NULL,
  `main_head_id` int(11) NOT NULL,
  `main_head` varchar(255) DEFAULT NULL,
  `account_head_id` int(11) NOT NULL,
  `account_head` varchar(255) NOT NULL,
  `opening_balance` double NOT NULL,
  `opening_balance_as_of` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `openingbalances`
--

INSERT INTO `openingbalances` (`id`, `main_head_id`, `main_head`, `account_head_id`, `account_head`, `opening_balance`, `opening_balance_as_of`, `created_at`, `updated_at`) VALUES
(1, 1, 'RECEIPTS', 1, 'ADMINISTRATION', 2000, '2018-06-01', '2018-06-14 11:01:58', '2018-06-14 04:16:15'),
(2, 1, 'RECEIPTS', 1, 'ADMINISTRATION', 2000, '2018-04-05', '2018-06-14 11:02:19', '2018-06-14 05:04:36'),
(3, 2, 'PAYMENTS', 6, 'EXAMINATION', 5000, '2018-03-06', '2018-06-14 11:03:41', '2018-06-14 05:10:27'),
(4, 1, 'RECEIPTS', 9, 'REVENUE RECEIPTS', 3000, '2018-02-01', '2018-06-14 11:04:52', '2018-06-14 05:27:23'),
(5, 2, 'PAYMENTS', 1, 'ADMINISTRATION', 2000, '2018-05-27', '2018-06-14 11:05:10', '2018-06-14 05:28:34'),
(6, 3, 'BANK ACCOUNTS', 17, 'APEX BANK OF INDIA (SILPUKHRI) 03084/24', 3000, '2018-06-11', '2018-06-14 05:29:44', '2018-06-14 05:29:44'),
(7, 1, 'RECEIPTS', 11, 'MISC RECEIPT', 2000, '2018-05-28', '2018-06-14 05:50:35', '2018-06-14 05:50:35'),
(8, 1, 'RECEIPTS', 11, 'MISC RECEIPT', 3000, '2018-05-27', '2018-06-14 06:06:09', '2018-06-14 06:06:09'),
(9, 4, 'GENERAL', 19, 'JAYA STORES', 3000, '2018-06-01', '2018-06-15 01:51:39', '2018-06-15 01:51:39');

-- --------------------------------------------------------

--
-- Table structure for table `subaccountheads`
--

CREATE TABLE `subaccountheads` (
  `id` int(11) NOT NULL,
  `sub_head` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subaccountheads`
--

INSERT INTO `subaccountheads` (`id`, `sub_head`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'Examination Permission Fees', 6, '2018-05-16 04:32:54', '2018-05-16 04:32:54'),
(2, 'Pass Certificate Fees', 6, '2018-05-16 04:33:56', '2018-05-16 04:33:56'),
(3, 'Center Change Fees', 6, '2018-05-16 04:35:03', '2018-05-16 04:35:03'),
(4, 'Re- Checking Fees', 6, '2018-05-16 04:35:18', '2018-05-16 04:35:18'),
(5, 'Examination Fees', 6, '2018-05-16 04:35:32', '2018-05-16 04:35:32'),
(6, 'Duplicate Fine Fees', 6, '2018-05-16 04:35:49', '2018-05-16 04:35:49'),
(7, 'Duplicate Admit Card Fees', 6, '2018-05-16 04:37:59', '2018-05-16 04:37:59'),
(8, 'Grant and Endowment Receipt', 9, '2018-05-16 04:38:54', '2018-05-16 04:38:54'),
(9, 'Endowment Receipt', 9, '2018-05-16 04:39:21', '2018-05-16 04:39:21'),
(10, 'Misc Receipt encashment of matured FDR', 10, '2018-05-16 04:39:41', '2018-05-16 04:39:41'),
(11, 'Interest from Security', 10, '2018-05-16 04:39:56', '2018-05-16 04:39:56'),
(12, 'Sales of old answer script/waste paper', 11, '2018-05-16 04:42:50', '2018-05-16 04:42:50'),
(13, 'Sales of old question paper and syllabus/vocationalbooks', 11, '2018-05-16 04:44:00', '2018-05-16 04:44:00'),
(14, 'Misc. Receipts including sale of Motor vehicle/disposable assets etc', 11, '2018-05-16 04:48:33', '2018-05-16 04:48:33'),
(15, 'Permission Fees', 12, '2018-05-16 04:49:06', '2018-05-16 04:49:06'),
(16, 'Recognition Fees', 12, '2018-05-16 04:49:21', '2018-05-16 04:49:21'),
(17, 'Enrolment Fees', 12, '2018-05-16 04:50:18', '2018-05-16 04:50:18'),
(18, 'Registration Fees', 12, '2018-05-16 04:50:52', '2018-05-16 04:50:52'),
(19, 'Eligibility Fees', 12, '2018-05-16 04:51:09', '2018-05-16 04:51:09'),
(20, 'Migration Fees', 12, '2018-05-16 04:51:23', '2018-05-16 04:51:23'),
(21, 'Royalty  ( Rcpt )', 12, '2018-05-16 04:52:51', '2018-05-16 04:52:51'),
(22, 'Duplicate Registration/Migration Certificate fees', 12, '2018-05-16 04:53:09', '2018-05-16 04:53:09'),
(23, 'Delay fine/Late fine', 12, '2018-05-16 10:38:40', '2018-05-16 04:53:28'),
(24, 'Registration Certificate correction fees', 12, '2018-05-16 10:39:50', '2018-05-16 04:54:06'),
(25, 'Change of Institution (Regn Branch)', 12, '2018-05-16 10:40:14', '2018-05-16 04:56:35'),
(26, 'haha ha aha lolllzzz  kkk', 20, '2018-05-16 10:58:32', '2018-05-16 05:28:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `id` int(10) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middlename` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/img/avatar.png',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `id`, `firstname`, `middlename`, `lastname`, `email`, `password`, `role`, `status`, `image`, `remember_token`, `created_at`, `updated_at`) VALUES
(8, 1008, 'DIGANTA', '', 'SARMA', 'diganta', '$2y$10$WOzoJD41sd3/PA1ugurGV.1EMxS5zXjDxLrHbjKw1ps3msQgtG2g2', '2', 1, '/img/avatar.png', 'hFrVH9T8EARsM7FbUuaaxyRrfd7wqi28fMgpNpXCMt6FNgHtIiVCw1TJs7tA', '2016-12-05 00:29:56', '2018-04-07 17:39:20'),
(9, 1040, 'BHUPEN', '', 'DOLEY', 'bhupen', '$2y$10$e2O4oNgiOgJy65CYIyIOA.wxJnm6KeExL5.Y7Cr1SNvjMpYVOsOsC', '3', 1, '/img/avatar.png', 'UnbuHNoruS52tnRw3fIHV7Gy445NDnifju4BSvuJkYMlSnsSrcI6kcq9M6Ur', '2017-11-02 05:44:47', '2018-03-06 14:39:50'),
(10, 2032, 'NABAJYOTI', '', 'BHARALI', 'nabajyoti', '$2y$10$pnE.ex4TWCycyBRR.iO9SO2G9aNcip9pKQ4TPpYiFp/BS7jl1KE2y', '1', 1, '/img/avatar.png', 'VLF2mcN88DIkfo53eMuWyRyzwQvmYcBrwWcGiehz3JYGdEL5VJggugPEdeg6', '2018-04-07 17:37:39', '2018-04-23 04:40:26'),
(12, 2041, 'Proloy', '', 'Roy', 'Proloy', '$2y$10$NcWueYsImPs9q7JnuvpMZufdeC3TARkoljXauALaLot9L6V/wO4Ey', '1', 1, '/img/avatar.png', 'iOduU7GCDkSsRX7jWxXfHRfIIdivaDZiHYtcyBYsWYJqsA6QfGNqYcXWSycy', '2018-04-23 04:40:10', '2018-04-23 05:19:10');

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL,
  `voucher_no` bigint(20) NOT NULL,
  `generated_voucher_no` varchar(255) NOT NULL,
  `voucher_type` varchar(50) NOT NULL,
  `voucher_date` date NOT NULL,
  `voucher_amount` double NOT NULL,
  `voucher_account_type` varchar(150) NOT NULL,
  `voucher_narration` text NOT NULL,
  `voucher_entry_date` date NOT NULL,
  `voucher_mode` varchar(50) NOT NULL,
  `account_head_id` int(11) NOT NULL,
  `dd_no` varchar(50) DEFAULT NULL,
  `challan_no` varchar(50) DEFAULT NULL,
  `dd_date` date DEFAULT NULL,
  `challan_date` date DEFAULT NULL,
  `bank_id` int(11) NOT NULL,
  `sub_head_id` int(11) NOT NULL,
  `debit` varchar(100) NOT NULL,
  `credit` varchar(100) NOT NULL,
  `utrno` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `voucher_no`, `generated_voucher_no`, `voucher_type`, `voucher_date`, `voucher_amount`, `voucher_account_type`, `voucher_narration`, `voucher_entry_date`, `voucher_mode`, `account_head_id`, `dd_no`, `challan_no`, `dd_date`, `challan_date`, `bank_id`, `sub_head_id`, `debit`, `credit`, `utrno`, `created_at`, `updated_at`) VALUES
(5, 999, 'FINMIS/2018-19/F/999', 'credit', '2018-04-01', 3000, 'receipt', 'Being amount receipt from  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic ipsam dolor molestiae reiciendis quae perspiciatis officia, ut, vel blanditiis id!', '2018-06-20', 'dd', 6, 'htyhy7678', NULL, '2018-05-02', NULL, 1, 4, 'EXAMINATION', 'Re- Checking Fees', NULL, '2018-06-20 08:45:34', '2018-06-20 03:15:34'),
(6, 67657, 'FINMIS/2018-19/F/67657', 'credit', '2018-05-07', 3000, 'receipt', 'Being amount receipt from  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic ipsam dolor molestiae reiciendis quae perspiciatis officia, ut, vel blanditiis id!', '2018-06-20', 'challan', 18, NULL, 'fv5t5tttttttty', NULL, '2018-05-14', 15, 17, 'KALITA STORES', 'Enrolment Fees', NULL, '2018-06-20 08:47:08', '2018-06-20 03:17:08'),
(7, 67657, 'FINMIS/2018-19/F/67657', 'credit', '2018-05-07', 3000, 'receipt', 'Being amount receipt from  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic ipsam dolor molestiae reiciendis quae perspiciatis officia, ut, vel blanditiis id!', '2018-06-20', 'challan', 18, NULL, 'fv5t5tttttttty', NULL, '2018-05-14', 15, 17, 'KALITA STORES', 'Enrolment Fees', NULL, '2018-06-20 08:47:53', '2018-06-20 03:17:53'),
(8, 67657, 'FINMIS/2018-19/F/67657', 'credit', '2018-05-07', 3000, 'receipt', 'Being amount receipt from  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic ipsam dolor molestiae reiciendis quae perspiciatis officia, ut, vel blanditiis id!', '2018-06-20', 'challan', 18, NULL, 'fv5t5tttttttty', NULL, '2018-05-14', 15, 17, 'KALITA STORES', 'Enrolment Fees', NULL, '2018-06-20 08:48:17', '2018-06-20 03:18:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accountheads`
--
ALTER TABLE `accountheads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accountopeningbalances`
--
ALTER TABLE `accountopeningbalances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounttransactions`
--
ALTER TABLE `accounttransactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounttransmasters`
--
ALTER TABLE `accounttransmasters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cashbooks`
--
ALTER TABLE `cashbooks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challans`
--
ALTER TABLE `challans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ddrafts`
--
ALTER TABLE `ddrafts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mainaccountheads`
--
ALTER TABLE `mainaccountheads`
  ADD PRIMARY KEY (`id`,`code_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `openingbalances`
--
ALTER TABLE `openingbalances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subaccountheads`
--
ALTER TABLE `subaccountheads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accountheads`
--
ALTER TABLE `accountheads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `accountopeningbalances`
--
ALTER TABLE `accountopeningbalances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `accounttransactions`
--
ALTER TABLE `accounttransactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `accounttransmasters`
--
ALTER TABLE `accounttransmasters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `cashbooks`
--
ALTER TABLE `cashbooks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `challans`
--
ALTER TABLE `challans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ddrafts`
--
ALTER TABLE `ddrafts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2042;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `openingbalances`
--
ALTER TABLE `openingbalances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `subaccountheads`
--
ALTER TABLE `subaccountheads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
