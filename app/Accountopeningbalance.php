<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accountopeningbalance extends Model
{
     protected $fillable = [
        'account_head_id',
        'opening_balance',
        'opening_balance_date',
        'voucher_id'
     ];

    public function accounthead()
    {
        return $this->belongsTo('App\Accounthead');
    }

    public function accounttransaction()
    {
        return $this->belongsTo('App\Accounttransaction');
    }

}
