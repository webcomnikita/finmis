<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Validator;

use App\Mainaccounthead;
use App\Accounthead;
use App\Openingbalance;

class AccountHeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $acheads = Accounthead::all();
        return view('accounthead.index',compact('acheads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $macheads = Mainaccounthead::all();
        return view('accounthead.create',compact('macheads'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'parent_id'               => 'required',
            'account_head'            => 'required'
        ];
        
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            Session::flash('error', 'Please fix the error and try again!');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $achead = new Accounthead();


        $mid = $request->input('parent_id');
        $main_ac_id = Mainaccounthead::where('id',$mid)->first();
        if (isset($main_ac_id->code_id)) {
            $macheads_code_id = $main_ac_id->code_id;
        }else
            $macheads_code_id = "";


        $achead->parent_id                          = $macheads_code_id;
        $achead->main_account_head_id               = $mid;



        $achead->account_head           = $request->input('account_head');
        $achead->save();

        Session::flash('success','You have successfully added account head');

        // return redirect()->route('view-all-account-head');
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $achead = Accounthead::find($id);
        $macheads = Mainaccounthead::all();
        return view('accounthead.edit',compact('achead','macheads'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $achead = Accounthead::find($id);

        $achead->parent_id              = $request->input('parent_id');
        $achead->account_head           = $request->input('account_head');
        $achead->save();

        Session::flash('success','You have successfully updated account head');

        return redirect()->route('view-all-account-head');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addOpeningBalance()
    {
        $macheads = Mainaccounthead::all();
        $opnbalnc = Openingbalance::all();
        return view('accounthead.opening-balance.create', compact('macheads','opnbalnc'));
    }

    public function getAccountHead(Request $request) 
    {
        $account_head_id = $request->input('main_head_id');

        if($account_head_id){
             $acheads = Accounthead::where('main_account_head_id',$account_head_id)->get();
             return response()->json($acheads);
        }
    }

    public function storeOpeningBalance(Request $request)
    {
       $rules = [
            'main_head_id'               => 'required',
            'account_head_id'            => 'required',
            'opening_balance'            =>'required',
            'opening_balance_as_of'       =>'required'
        ];
        
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            Session::flash('error', 'Please fix the error and try again!');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $opnbalnc = new Openingbalance();


        $mhid = $request->input('main_head_id');
        $main_ac_head = Mainaccounthead::where('id',$mhid)->first();
        if (isset($main_ac_head->major_account_head)) {
            $macheads_ac_head = $main_ac_head->major_account_head;
        }else
            $macheads_ac_head = "";


        $opnbalnc->main_head_id                       = $mhid;
        $opnbalnc->main_head                          = $macheads_ac_head;


        $macid = $request->input('account_head_id');
        $ac_head = Accounthead::where('id',$macid)->first();
        if (isset($ac_head->account_head)) {
            $acheads_ac_head = $ac_head->account_head;
        }else
            $acheads_ac_head = "";


        $opnbalnc->account_head_id                       = $macid;
        $opnbalnc->account_head                          = $acheads_ac_head;


        $opnbalnc->opening_balance           = $request->input('opening_balance');

        $as_of_date = $request->input('opening_balance_as_of');
        $tdr = str_replace("/", "-", $as_of_date);
        $asofDate = date('Y-m-d',strtotime($tdr));

        $opnbalnc->opening_balance_as_of     = $asofDate;
        $opnbalnc->save();

        Session::flash('success','You have successfully added opening balance');

        return redirect()->back();
    }

}
