<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Validator;
use Carbon\Carbon;
use App\Accounthead;
use App\Bankmaster;
use App\Subaccounthead;
use App\Voucher;
use App\Accounttransaction;
use App\Accounttransmaster;
use App\Ddraft;
use App\Challan;

class VVoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vouchers = Voucher::all();
        return view('voucher.index',compact('vouchers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $acheads = Accounthead::all();
        //$subacheads = Subaccounthead::where('parent_id', '=', $acheads)->get();
        //$subacheads = Accounthead::with(['subaccounthead'])->get();
        $subacheads = Subaccounthead::all();
        $bank = Bankmaster::all();
        return view ('voucher.create',compact('bank','acheads','subacheads'));
    }


    public function getSubAccountHead(Request $request) 
    {
        $sub_account_head_id = $request->input('account_head_id');

        if($sub_account_head_id){
             $subacheads = Subaccounthead::where('parent_id',$sub_account_head_id)->get();
             return response()->json($subacheads);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        // $rules = [
        //     'voucher_no'              => 'required',
        //     'voucher_date'            => 'required',
        //     'voucher_amount'          => 'required',
        //     'voucher_narration'       => 'required',
        //     'voucher_entry_date'      => 'required',
        //     'voucher_mode'            => 'required'

        // ];
        
        // if ($request->input('voucher_type') == 'receipt')

        // {
        //     $rules = [
        //     'voucher_no'              => 'required',
        //     'voucher_date'            => 'required',
        //     'voucher_amount'          => 'required',
        //     'voucher_narration'       => 'required',
        //     'voucher_entry_date'      => 'required',
        //     'voucher_mode'            => 'required',

        //     'sub_head_id'             => 'required'

        // ];

        // }

        // $validator = Validator::make($request->all(), $rules);

        // if ($validator->fails()) {
        //     Session::flash('error', 'Please fix the error and try again!');
        //     return redirect()->back()->withErrors($validator)->withInput();
        // }
  
        $voucher = new Voucher();

        foreach($request->input('voucher_type') as $key => $value ){
            if ($value === null) {
                continue;
            }
        
        $vno   = $request->input('voucher_no');

        $voucher->voucher_no       = $vno;




        // $icode = str_random(5);
        $cdate = date("Y");
        $nxtyr = date('y', strtotime('+1 year'));
        $gvno = 'FINMIS'.'/'.$cdate.'-'.$nxtyr.'/'.'F'.'/'.$vno;

        $voucher->generated_voucher_no     = $gvno;



        $vou_date = $request->input('voucher_date');
        $tdr = str_replace("/", "-", $vou_date);
        $vDate = date('Y-m-d',strtotime($tdr));

        $voucher->voucher_date          =  $vDate;
      

        $today1  = date("Y-m-d");
        $voucher->voucher_entry_date    = $today1;

        // $voucher->voucher_amount        = $request->input('voucher_amount');
        
        $voucher->voucher_narration     = $request->input('voucher_narration');
        $voucher->voucher_type          = $request->input('voucher_type')[$key];

        $ac = $request->input('account_head_id')[$key];
        $acheads = Accounthead::where('id',$ac)->first();
        if (isset($acheads->account_head)) {
            $acheads_name = $acheads->account_head;
        }else
            $acheads_name = "";


        $voucher->account_head_id        = $ac;
        $voucher->debit                  = $acheads_name;


        $sbac = $request->input('sub_head_id')[$key];
        $scheads = Subaccounthead::where('id',$sbac)->first();
        if (isset($scheads->sub_head)) {
            $scheads_name = $scheads->sub_head;
        }else
            $scheads_name = "";
    


        $voucher->sub_head_id            = $sbac;
        $voucher->credit                 = $scheads_name;
         
        
        $voucher->voucher_amount          = $request->input('debit')[$key];
        

        $voucher->voucher_mode          = $request->input('voucher_mode');
        if ($request->input('voucher_mode') == 'cash' || 'dd' || 'challan') {
           $voucher->bank_id            = $request->input('bank_id');
        }
        if ($request->input('voucher_mode') == 'dd') {
           $voucher->dd_no              = $request->input('dd_no');
           $voucher->dd_date            = $request->input('dd_date');
        }
        if ($request->input('voucher_mode') == 'challan') {
           $voucher->challan_no         = $request->input('challan_no');
           $voucher->challan_date       = $request->input('challan_date');
        }

        if ($request->input('voucher_mode') == 'onlinepay') {
            

            $voucher->utrno         = $request->input('utrno');
        }

        $voucher->voucher_account_type    = $request->input('voucher_account_type');
      
        $voucher->save();




        

        $actrans = new Accounttransaction();
       

            //$actrans->voucher_id     =  $voucher->id;

            $vid = $voucher->id;
            $vno = Voucher::where('id',$vid)->first();
            $vou_no = $vno->voucher_no;

            $actrans->voucher_id              = $vid;
            $actrans->voucher_no              = $vou_no;


            
            $actrans->voucher_date            = $voucher->voucher_date;
            $actrans->voucher_account_type    = $voucher->voucher_account_type;
            $actrans->generated_voucher_no    = $voucher->generated_voucher_no;




            $actrans->trans_type     =  $request->input('voucher_type')[$key];


            $ac = $request->input('account_head_id')[$key];
            $acheads = Accounthead::where('id',$ac)->first();
            $acheads_name = $acheads->account_head;

            $actrans->debit_id                = $ac;
            $actrans->debit_name              = $acheads_name;




            $sbac = $request->input('sub_head_id')[$key];
            $scheads = Subaccounthead::where('id',$sbac)->first();
            $scheads_name = $scheads->sub_head;
        


            $actrans->credit_id               = $sbac;
            $actrans->credit_name             = $scheads_name;


            $actrans->v_amount          =  $request->input('debit')[$key];

            $actrans->save();        

        }

        if($voucher->voucher_mode =='dd'){
           $dd = new Ddraft();

            $vid = $voucher->id;
            $vno = Voucher::where('id',$vid)->first();
            $vou_no = $vno->voucher_no;
            $vg_no = $vno->generated_voucher_no;
            $dno = $vno->dd_no;
            $ddt = $vno->dd_date;

            $dd->voucher_id              = $vid;
            $dd->voucher_no              = $vou_no;
            $dd->generated_voucher_no    = $vg_no;
            $dd->dd_no                   = $dno;
            $dd->dd_date                 = $ddt;

            $vbid = $voucher->bank_id;
            $vbnk = Bankmaster::where('id',$vbid)->first();
            $vbname = $vbnk->bankname;

            $dd->bank_id              = $vbid;
            $dd->bank_name             = $vbname;

            $dd->save();  
        }
        



    if($voucher->voucher_mode =='challan'){
        $challan = new Challan();

        $cid = $voucher->id;
        $cno = Voucher::where('id',$cid)->first();
        $vouc_no = $cno->voucher_no;
        $vcg_no = $cno->generated_voucher_no;
        $chno = $cno->challan_no;
        $challandt = $cno->challan_date;

        $challan->voucher_id              = $cid;
        $challan->voucher_no              = $vouc_no;
        $challan->generated_voucher_no    = $vcg_no;
        $challan->challan_no                   = $chno;
        $challan->challan_date                 = $challandt;

        $vcbid = $voucher->bank_id;
        $vcbnk = Bankmaster::where('id',$vcbid)->first();
        $vcbname = $vcbnk->bankname;

        $challan->bank_id              = $vcbid;
        $challan->bank_name             = $vcbname;

        $challan->save();
    }


       $actransmaster = new Accounttransmaster();

       $actransmaster->voucher_id   =  $voucher->id;
       $actransmaster->entry_no           =  $voucher->voucher_no;
       $actransmaster->voucher_narration  =  $voucher->voucher_narration;
       $actransmaster->voucher_amount     =  $voucher->voucher_amount;
       $actransmaster->entered_by         =  $voucher->voucher_mode;
       $actransmaster->entered_on         =  $voucher->voucher_entry_date;
       $actransmaster->save();

       Session::flash('success','You have successfully added voucher details');

        return redirect()->route('view-all-voucher');

    }
       

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $voucher = Voucher::where('id', $id)->first();

        $bnme= Voucher::where('id', $id)->first()->bank_id;
        $bname = Bankmaster::where('id', $bnme)->first()->bankname;

        $achd = Voucher::where('id', $id)->first()->account_head_id;
        $acheadname = Accounthead::where('id', $id)->first()->account_head;

        $sachd = Voucher::where('id', $id)->first()->sub_head_id;
        $sacheadname = Subaccounthead::where('id', $id)->first()->sub_head;

        return view('voucher.single',compact('voucher','bname','acheadname','sacheadname'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $voucher = Voucher::find($id);
        $acheads = Accounthead::all();
        $subacheads = Subaccounthead::all();
        $bank = Bankmaster::all();
        return view('voucher.edit', compact('voucher','acheads','bank','subacheads'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $voucher = Voucher::find($id);

      

        $voucher->voucher_no            = $request->input('voucher_no');

        $vou_date = $request->input('voucher_date');
        $tdr = str_replace("/", "-", $vou_date);
        $vDate = date('Y-m-d',strtotime($tdr));

        $voucher->voucher_date          =  $vDate;

        $today1 = date("Y-m-d");
        

        $voucher->voucher_entry_date    = $today1;

        $voucher->voucher_amount        = $request->input('voucher_amount');
        
        $voucher->voucher_narration     = $request->input('voucher_narration');
        $voucher->voucher_type          = $request->input('voucher_type')[$key];

        $ac = $request->input('account_head_id')[$key];
        $acheads = Accounthead::where('id',$ac)->first();
        $acheads_name = $acheads->account_head;
       


        $voucher->account_head_id        = $ac;
        $voucher->debit                  = $acheads_name;


        $sbac = $request->input('sub_head_id')[$key];
        $scheads = Subaccounthead::where('id',$sbac)->first();
        $scheads_name = $scheads->sub_head;
    


        $voucher->sub_head_id            = $sbac;
        $voucher->credit                 = $scheads_name;
         
        
        $voucher->voucher_amount          = $request->input('debit')[$key];
        

        $voucher->voucher_mode          = $request->input('voucher_mode');
        if ($request->input('voucher_mode') == 'cash' || 'dd' || 'challan') {
           $voucher->bank_id            = $request->input('bank_id');
        }
        if ($request->input('voucher_mode') == 'dd') {
           $voucher->dd_no              = $request->input('dd_no');
           $voucher->dd_date            = $request->input('dd_date');
        }
        if ($request->input('voucher_mode') == 'challan') {
           $voucher->challan_no         = $request->input('challan_no');
           $voucher->challan_date       = $request->input('challan_date');
        }

        if ($request->input('voucher_mode') == 'onlinepay') {
            

            $voucher->utrno         = $request->input('utrno');
        }

          $voucher->save();
       


       Session::flash('success','You have successfully updated your voucher details');

       return redirect()->route('view-all-voucher');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function trypage()
    {
        $acheads = Accounthead::all();
        $subacheads = Subaccounthead::all();
        return view('voucher.try', compact('acheads','subacheads'));
    }
   
}
