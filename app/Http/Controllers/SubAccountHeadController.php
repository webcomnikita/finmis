<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Validator;
use DB;


use App\Accounthead;
use App\Subaccounthead;

class SubAccountHeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $sbacheads = Subaccounthead::all();

        return view('subaccounthead.index',compact('sbacheads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $acheads = Accounthead::all();
        return view('subaccounthead.create',compact('acheads'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'parent_id'               => 'required',
            'sub_head'            => 'required'
        ];
        
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            Session::flash('error', 'Please fix the error and try again!');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $sbachead = new Subaccounthead();

        $sbachead->parent_id              = $request->input('parent_id');
        $sbachead->sub_head           = $request->input('sub_head');
        $sbachead->save();

        Session::flash('success','You have successfully added sub account head');

        return redirect()->route('view-sub-all-account-head');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sbachead = Subaccounthead::find($id);
        $acheads = Accounthead::all();
        return view('subaccounthead.edit',compact('acheads','sbachead'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sbachead = Subaccounthead::find($id);

        $sbachead->parent_id              = $request->input('parent_id');
        $sbachead->sub_head           = $request->input('sub_head');
        $sbachead->save();

        Session::flash('success','You have successfully updated sub account head');

        return redirect()->route('view-sub-all-account-head');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
