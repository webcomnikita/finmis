<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Accounthead;
use App\Voucher;
use App\Accounttransaction;
use App\Subaccounthead;


class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function headWiseView()
    {
        $acheads = Accounthead::all();
        return view('dashboard.head-wise.head-wise-report' ,compact('acheads'));
    }

    public function headWiseSubhead($id)
    {
        $acheads = Accounthead::where('id', $id)->first()->id;
        $vdetails = Accounttransaction::where('debit_id', $acheads)->get();
                      
        return view('dashboard.head-wise.head-wise-subhead',compact('vdetails'));
    }

    public function headWiseSubheadDetails($credit_id)
    {
        $sacheads = Accounttransaction::where('credit_id', $credit_id)->first()->voucher_id;
        $voucher = Voucher::where('id', $sacheads)->get();

        return view('dashboard.head-wise.head-wise-subhead-details',compact('voucher'));
    }

    public function headWiseSubheadDetailsFull($id)
    {
        $voucher = Voucher::where('id', $id)->first()->id;
      
        $vtrans = Accounttransaction::where('voucher_id',$voucher)->get();
        $vnrtn = Voucher::where('id', $voucher)->first()->voucher_narration;
        $vnratn = Voucher::where('id', $voucher)->first()->voucher_account_type;


        $vno = Accounttransaction::where('voucher_id',$voucher)->first()->voucher_no;
        $vgnrtd = Voucher::where('id', $voucher)->first()->generated_voucher_no;
        $vdate = Voucher::where('id', $voucher)->first()->voucher_entry_date;


        $vinwords = Voucher::where('id', $voucher)->first()->voucher_amount;


        // $inWords = new \NumberFormatter('en', \NumberFormatter::SPELLOUT);
        // $amount_words = $inWords->format($vinwords);


        // echo $amount_words;
        // die();

        return view('dashboard.head-wise.head-wise-subhead-details-full',compact('vtrans','vnrtn','vnratn','vinwords','vno','vgnrtd','vdate'));
    }

    public function dayWiseViewFilter()
    {
       return view('dashboard.day-wise.index');
    }

    public function dayWiseView(Request $request)
    {
        $start = $request->input('from_date');
        $end = $request->input('to_date');

        $from = min($start, $end);
        $till = max($start, $end);
        // dd($till);
        // dd($from);
       

        // $filter = Accounttransaction::where('created_at','>=', $start)->where('created_at','<=', $end)->get();

         $filter = Accounttransaction::whereBetween('voucher_date',[$from,$till])->get();

        // dd($filter);
        return view('dashboard.day-wise.day-wise-report',compact('filter'));
    }


    public function dayWiseSubheadDetailsFull($id)
    {
        $vcher = Accounttransaction::where('id', $id)->first()->voucher_id;
        $vno = Accounttransaction::where('id', $id)->first()->voucher_no;
        $vgnrtd = Accounttransaction::where('id', $id)->first()->generated_voucher_no;
        $vdate = Accounttransaction::where('id', $id)->first()->created_at;

        $vtran = Accounttransaction::where('voucher_id',$vcher)->get();

        $vnrrtn = Voucher::where('id', $vcher)->first()->voucher_narration;
        $vnratn = Voucher::where('id', $vcher)->first()->voucher_account_type;
        // $vinwords = Voucher::where('id', $vcher)->first()->voucher_amount;
        $vinwords = Accounttransaction::where('voucher_id',$vcher)
                                        ->where('trans_type','=','debit')->first()->v_amount;

                           
                                        // dd($vinwords);
                                        // die();



        return view('dashboard.day-wise.day-wise-report-full',compact('vtran','vnratn','vinwords','vnrrtn','vno','vgnrtd','vdate'));
    }

    public function receiptPaymentWiseFilter()
    {
        $filterreceipt = Accounttransaction::where('trans_type','debit')->get();
        $filterpayment = Accounttransaction::where('trans_type','credit')->get();

        return view('dashboard.receipt-payment.receipt-payment-wise-report',compact('filterreceipt','filterpayment'));
    }

    public function receiptPaymentWise(Request $request)
    {
        $start = $request->input('from-date');
        $end = $request->input('to-date');

        $from = min($start, $end);
        $till = max($start, $end);
        
        $filterdatewisereceipt = Accounttransaction::whereBetween('voucher_date',[$from,$till])->where('trans_type','debit')->get();

         $filterdatewisepayment = Accounttransaction::whereBetween('voucher_date',[$from,$till])->where('trans_type','credit')->get();

        
        return view('dashboard.receipt-payment.receipt-payment-wise-report-full',compact('filterdatewisereceipt','filterdatewisepayment'));
    }

}
