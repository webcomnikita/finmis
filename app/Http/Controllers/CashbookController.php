<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Accounttransaction;
use App\Accountopeningbalance;
use App\Accounthead;
use App\Voucher;

class CashbookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('cashbook.iindex-view-filter'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function viewFilterPage(Request $request)
    {
      
        $abc = $request->input('s_date');
        // $result = Accountopeningbalance::with('accounthead')->get();
        $accopnbals = Accountopeningbalance::where('opening_balance_date', $abc)->get();

        return view('cashbook.index',compact('accopnbals')); 
    }

    public function addCashbook()
    {
        $acheads = Accounthead::all();
        $vouchers = Voucher::all();
        return view('cashbook.add-cashbook',compact('acheads','vouchers'));
    }

    public function storeCashbook(Request $request)
    {
        
    }
}
