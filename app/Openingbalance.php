<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Openingbalance extends Model
{
    protected $fillable = [
        'main_head_id',
        'main_head',
        'account_head_id',
        'account_head',
        'opening_balance',
        'opening_balance_as_of'
     ];
}
