<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mainaccounthead extends Model
{
    protected $fillable = [
        'code_id',
        'major_account_head'
    ];
}
