<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounttransmaster extends Model
{
    protected $fillable = [
        'voucher_id',
        'entry_no',
        'voucher_narration',
        'voucher_amount',
        'entered_by',
        'entered_on'
     ];
}
