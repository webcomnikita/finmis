<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subaccounthead extends Model
{
    protected $fillable = [
        'sub_head_id',
        'sub_head',
        'parent_id'
    ];

    public function accounthead()
    {
        return $this->belongsTo('App\Accounthead');
    }
}
