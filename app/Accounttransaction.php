<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounttransaction extends Model
{
    protected $fillable = [
        'voucher_id',
        'voucher_no',
        'generated_voucher_no',
        'voucher_date',
        'voucher_account_type',
        'debit_id ',
        'debit_name',
        'credit_id',
        'credit_name',
        'v_amount',
        'trans_type'
     ];

     public function accountopeningbalance()
    {
        return $this->hasMany('App\Accountopeningbalance');
    }

     public function accounthead()
    {
        return $this->belongsTo('App\Accounthead');
    }

    public function voucher()
    {
        return $this->belongsTo('App\Accounthead');
    }
    
}
