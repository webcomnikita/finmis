<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Challan extends Model
{
     protected $fillable = [
        'voucher_id',
        'voucher_no',
        'generated_voucher_no',
        'challan_no',
        'challan_date',
        'bank_id',
        'bank_name',
        'deposit'
     ];
}
