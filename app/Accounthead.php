<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounthead extends Model
{
    protected $fillable = [
        'main_account_head_id',
        'account_head',
        'parent_id',
        'grand_parent_id'
     ];

    public function subaccounthead()
    {
        return $this->hasMany('App\Subaccounthead');
    }

    public function accountopeningbalance()
    {
        return $this->hasMany('App\Accountopeningbalance');
    }

    public function accounttransaction()
    {
        return $this->hasMany('App\Accounttransaction');
    }

    public function voucher()
    {
        return $this->hasMany('App\Voucher');
    }

    
}
