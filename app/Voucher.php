<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $fillable = [
        'voucher_no',
        'generated_voucher_no',
        'voucher_type',
        'voucher_date',
        // 'voucher_amount',
        'voucher_account_type',
        'voucher_narration',
        'voucher_entry_date',
        'voucher_mode',
        'account_head_id',
        'dd_no',
        'challan_no',
        'dd_date',
        'challan_date',
        'bank_id',
        'sub_head_id',
        
    ];

    public function accounthead()
    {
        return $this->belongsTo('App\Accounthead');
    }

    // public function accounttransaction()
    // {
    //     return $this->hasMany('App\Accounttransaction');
    // }
}
