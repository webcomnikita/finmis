<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bankmaster extends Model
{
    protected $fillable = [
        'bankname'
     ];
}
