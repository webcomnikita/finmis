<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ddraft extends Model
{
    protected $fillable = [
        'voucher_id',
        'voucher_no',
        'generated_voucher_no',
        'dd_no',
        'dd_date',
        'bank_id',
        'bank_name',
        'deposit'
     ];
}
