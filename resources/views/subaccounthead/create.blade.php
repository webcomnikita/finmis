@extends('layouts.front')


@section('styles')
@stop


@section('content') 
   
<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Add new account head</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form method="post" action="{{ route('add-sub-account-head.post') }}">
					{{ csrf_field() }}
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">		
								<div class="form-group">
									<label for="parent_id">Account head</label>
									<select class="form-control" name="parent_id" id="parent_id">
										<option value="">-- Select your account head --</option>
										<?php foreach ($acheads as $achead): ?>
											<option value="{{ $achead->id }}" {{ (old('parent_id') == $achead->id) ? 'selected' : '' }}>{{ $achead->account_head }}</option>
										<?php endforeach; ?>
									</select> 

								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="sub_head">Sub account head</label>
									<input type="text" name="sub_head" id="sub_head" class="form-control" placeholder="Enter sub account head name">
								</div>
							</div>

							<div class="col-md-12">
								<button type="submit" class="btn btn-primary btn-block">Submit</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>


@endsection


@section('scripts')

@stop
  