@extends('layouts.front')


@section('styles')
<link rel="stylesheet" href="{!!asset('assets/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}">
<style>
 .btn-pink {
  background-color: rgba(135, 75, 188, 0.9);
  color: #fff;
 }
 .btn-pink:hover {
  background-color: rgba(135, 75, 188, 1);
  color: #fff;
 }
</style>
@stop


@section('content') 
   
<!-- Main content -->
<section class="content">

<form method="post" action="{{ route('add-opening-balance.post') }}" onsubmit="return confirm('Please confirm that the following details are correct')">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Add new opening balance</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">		
								<div class="form-group">
									<label for="main_head_id">Major head</label>
									<select class="form-control" name="main_head_id" id="main_head_id" required="required">
										<option value="">-- Select your major account head --</option>
										<?php foreach ($macheads as $machead): ?>
											<option value="{{ $machead->id }}" data-themeid="{{ $machead->id }}">{{ $machead->major_account_head }}</option>
										<?php endforeach; ?>
									</select> 

								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="account_head_id">Account head</label>
	                                <select id="account_head_id" name="account_head_id" class="form-control" required="required">
	                                	<option value=""></option>
	                                </select>
	                          </div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="opening_balance">Opening balance</label>
									<input type="text" name="opening_balance" id="opening_balance" class="form-control" placeholder="Enter opening balance" required="required">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="opening_balance_as_of">As of</label>
									<input type="date" name="opening_balance_as_of" id="opening_balance_as_of" class="form-control" required="required">
								</div>
							</div>


							<div class="col-md-12">
								<a data-toggle="modal" id="view-modal" class="btn btn-pink btn-block" data-backdrop="static" data-keyboard="false">Preview</a>
							</div>
						</div>
					</div>
				
			</div>
		</div>
	</div>

<!-- Modal -->
  <div class="modal fade" id="confirm-submit" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Opening balance details</h4>
        </div>
        <div class="modal-body">
          <table class="table">
	            <tr>
	                <th>Major head : </th>
	                <td id="mhead"></td>
	            </tr>
	             <tr>
	                <th>Account head : </th>
	                <td id="ahead"></td>
	            </tr>
	             <tr>
	                <th>Opening balance : </th>
	                <td id="obal"></td>
	            </tr>
	             <tr>
	                <th>As of date : </th>
	                <td id="asof" class="shortDateFormat"></td>
	            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </div>
      
    </div>
  </div>


</form>


@if($opnbalnc)
	<!-- View all opening balance -->
	 <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">View all opening balance</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="opblnc-details" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Major account head</th>
                  <th>Account head</th>
                  <th>Opening balance</th>
                  <th>As of date</th>
                  <!-- <th>Action</th> -->
                </tr>
                </thead>
                <tbody>
            	<?php $i=1; ?>
                <?php foreach ($opnbalnc as $opb): ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td>{{ $opb->main_head }}</td>
                  <td>{{ $opb->account_head }}</td>
                  <td><i class="fa fa-rupee"></i> {{ $opb->opening_balance }}</td>
                  <td>{{  date('d-M-Y', strtotime($opb->opening_balance_as_of)) }}</td>
                  <!-- <td><a href="" class="btn btn-success" title="View details">
                      <i class="fa fa-eye"></i></a></td> -->
                </tr>
                <?php $i++; ?>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Major account head</th>
                  <th>Account head</th>
                  <th>Opening balance</th>
                  <th>As of date</th>
                  <!-- <th>Action</th> -->
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          @else
     No opening balance added yet!
@endif
</section>
<!-- /.content -->



@endsection


@section('scripts')
<script src="{!!asset ('assets/plugins/datatables.net/js/jquery.dataTables.min.js') !!}"></script>
<script src="{!!asset ('assets/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}"></script>

<!-- <script src="{!!asset ('assets/js/jquery.dateFormat.js') !!}"></script> -->
<script>
$(document).ready(function() {

	$('#opblnc-details').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })

 $("#main_head_id").change(function(){
	//var theme_id = $(this).val();
	var main_head_id = $('option:selected', this).attr('data-themeid');

	$.ajax({
	  type: "POST",
	  url: "{{ route('getaccountheads.ajax.post') }}",
	  data: {
	    'main_head_id': main_head_id
	  },

	  success: function(response) {
	    if(response) {

	      var toAppend = '';

	      $.each(response, function(i,o){
	        toAppend += '<option value="'+o.id+'">'+o.account_head+'</option>';
	      });

	      $('#account_head_id').html(toAppend);

	    } else {
	      alert("No account heads found");
	    }
	  }
	})
  });
});


// 	var a = document.getElementById("main_head_id").value;
// 	var b = document.getElementById("account_head").value;
// 	// var c = document.getElementById("opening_balance").value;
// 	// var d = document.getElementById("opening_balance_as_of").value;

//     if(!confirm('Please confirm that the following details are correct:' )){
//         e.preventDefault();
//         return false;
//     }
//     return true;

$("#view-modal").click(function(e){
	$("#confirm-submit").modal({
      backdrop: 'static',
      keyboard: false
    });

 var sel = document.getElementById('main_head_id');
 var opt = sel.options[sel.selectedIndex];
 var majorhd = opt.text ;
 $('#mhead').text(majorhd);

 var sela = document.getElementById('account_head_id');
 var opta = sela.options[sela.selectedIndex];
 var ajorhd = opta.text ;
 $('#ahead').text(ajorhd);

 $('#obal').text($('#opening_balance').val());


 var a = $('#asof').text($('#opening_balance_as_of').val());


});





// $('[data-toggle=confirmation]').confirmation({
//   rootSelector: '[data-toggle=confirmation]',
//   // other options
// });


</script>
@stop
  