@extends('layouts.front')


@section('styles')
@stop


@section('content') 
   
<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Add new account head</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form method="post" action="{{ route('add-account-head.post') }}">
					{{ csrf_field() }}
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">		
								<div class="form-group">
									<label for="parent_id">Major head</label>
									<select class="form-control" name="parent_id" id="parent_id">
										<option value="">-- Select your major account head --</option>
										<?php foreach ($macheads as $machead): ?>
											<option value="{{ $machead->id }}" {{ (old('parent_id') == $machead->id) ? 'selected' : '' }}>{{ $machead->major_account_head }}</option>
										<?php endforeach; ?>
									</select> 

								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="account_head">Account head</label>
									<input type="text" name="account_head" id="account_head" class="form-control" placeholder="Enter account head name">
								</div>
							</div>

							<div class="col-md-12">
								<button type="submit" class="btn btn-primary btn-block">Submit</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>


@endsection


@section('scripts')

@stop
  