<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin FINMIS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="_token" content="{!! csrf_token() !!}"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{!!asset('assets/plugins/font-awesome/css/font-awesome.min.css') !!}">
  <!--Sweetalert2 plugin-->
  <link rel="stylesheet" href="{!! asset('assets/plugins/sweetalert2/sweetalert2.css') !!}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{!!asset('assets/plugins/Ionicons/css/ionicons.min.css') !!}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{!!asset('assets/css/AdminLTE.min.css') !!}">
 
  <link rel="stylesheet" href="{!!asset('assets/css/skins/skin-blue.min.css') !!}">

  <link rel="stylesheet" type="text/css" href="{!!asset('assets/css/custom.css') !!}">
 
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
        @yield('styles')
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>F</b>IN</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>FINMIS</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
         
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
             <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">Finmis</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                
                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
             
              <li class="user-footer">
               
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>FINMIS</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

     

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">HEADER</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="{!! Request::path() == '/' ? 'active' : '' !!}"><a href="{{ route('dashboard') }}"><i class="fa fa-link"></i> <span>Dashboard</span></a></li>
        <!-- <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li> -->
       

        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Voucher</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>

          <ul class="treeview-menu">
            <li class="{!! Request::path() == 'add-voucher' ? 'active' : '' !!}"><a href="{{ route('add-voucher') }}">Add</a></li>
            <li class="{!! Request::path() == 'view-voucher' ? 'active' : '' !!}"><a href="{{ route('view-all-voucher') }}">View all</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Account Head</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{!! Request::path() == 'add-account-head' ? 'active' : '' !!}"><a href="{{ route('add-account-head') }}">Add</a></li>
            <li class="{!! Request::path() == 'view-account-head' ? 'active' : '' !!}"><a href="{{ route('view-all-account-head') }}">View all</a></li>
            <li class="{!! Request::path() == 'add-opening-balance' ? 'active' : '' !!}"><a href="{{ route('add-opening-balance') }}">Add opening balance</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Sub Account Head</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>

          {{ request()->is('sites/*/edit') ? 'active' : '' }}
          <ul class="treeview-menu">
            <li class="{!! Request::path() == 'add-sub-account-head' ? 'active' : '' !!}"><a href="{{ route('add-sub-account-head') }}">Add</a></li>
            <li class="{!! Request::path() == 'view-sub-account-head' ? 'active' : '' !!}"><a href="{{ route('view-sub-all-account-head') }}">View all</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Cashbook/Bankbook</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="{!! Request::path() == 'add-cashbook' ? 'active' : '' !!}"><a href="{{ route('add-cashbook') }}">Add cashbook </a></li>
            <li><a href="#">Add bankbook </a></li>
            <li class="{!! Request::path() == 'select-cashbook' ? 'active' : '' !!}"><a href="{{ route('select-cashbook') }}">View all</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Ledger</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Add </a></li>
            <li><a href="#">View all</a></li>
          </ul>
        </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">



   @yield('content')



  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="#">FINMIS</a>.</strong> All rights reserved.
  </footer>
 
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{!!asset('assets/plugins/jquery/dist/jquery.min.js') !!}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.7/bootstrap-confirmation.js"></script> -->
<!-- AdminLTE App -->
<script src="{!!asset('assets/js/adminlte.min.js') !!}"></script>
<script type="text/javascript" src="{!!asset('assets/js/js.cookie.js') !!}"></script>
<script>
    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
</script>
<script type="text/javascript" src="{!! asset('assets/plugins/sweetalert2/sweetalert2.min.js') !!}"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        @if (Session::has('error'))

        swal(
            'Error',
            '{{ Session::get("error") }}',
            'error'
            );

        @endif

        @if (Session::has('success'))

        swal(
            'Success',
            '{{ Session::get("success") }}',
            'success'
            );

        @endif
    });
    </script>

@yield('scripts')
</body>
</html>