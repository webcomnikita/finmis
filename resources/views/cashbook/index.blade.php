@extends('layouts.front')


@section('styles')
<link rel="stylesheet" href="{!!asset('assets/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}">
@stop


@section('content') 
   
    <!-- Main content -->
    <section class="content container-fluid">

     <div class="box">
            <div class="box-header">
              <h3 class="box-title">View all account head</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="v-details" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Account head</th>
                  <th>Amount</th>
                  <th>Date</th>
                  <th>Voucher</th>
                </tr>
                </thead>
                <tbody>
              <?php $i=1; ?>
                <?php foreach ($accopnbals as $accopnbal): ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td>{{ $accopnbal->accounthead->account_head }}</td>
                  <td>{{ $accopnbal->opening_balance }}</td>
                  <td>{{ date('Y-M-d', strtotime($accopnbal->opening_balance_date)) }}</td>
                  <td>{{ $accopnbal->voucher_no }}</td>
                </tr>
                <?php $i++; ?>
                <?php endforeach; ?>
                </tbody>
                <!-- <tfoot>
                <tr>
                  <th>#</th>
                  <th>Account head</th>
                  <th>Amount</th>
                  <th>Date</th>
                  <th>Voucher</th>
                </tr>
                </tfoot> -->
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->

    @endsection


@section('scripts')
<script src="{!!asset ('assets/plugins/datatables.net/js/jquery.dataTables.min.js') !!}"></script>
<script src="{!!asset ('assets/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}"></script>
<script>
  $(function () {
    $('#v-details').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })
  })
</script>
@stop
  