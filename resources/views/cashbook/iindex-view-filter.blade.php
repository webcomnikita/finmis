@extends('layouts.front')


@section('styles')
<style type="text/css">
	.prnt {
		position: relative;
		width: 100%;
		/*background-color: #eee;*/
		

	}
	.chld {
		position: relative;
		width: 60%;
		padding: 1em;
		background-color: #fff;
		margin: 0 auto;
		vertical-align: middle;
		
	}
</style>
@stop


@section('content') 
   
<div class="prnt">
	<div class="chld">
		<form action="{{ route('view-cashbook.post') }}" method="post">
			{{ csrf_field() }}
			<div class="row">
				<div class="col-md-12">
					<label>Select date</label>
					
					<div class="input-group">
						<input type="date" class="form-control" name="s_date" required="required">
						<div class="input-group-btn">
							<button class="btn btn-warning" type="submit"><i class="glyphicon glyphicon-search"></i></button>
						</div>
					</div>
					
				</div>	
			</div>
		</form>
	</div>
</div>    

@endsection


@section('scripts')

@stop
  