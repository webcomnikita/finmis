@extends('layouts.front')


@section('styles')
<link rel="stylesheet" href="{!! asset('assets/plugins/select2/dist/css/select2.min.css') !!}">
<style>
.select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #ccc !important;
    border-radius: 0 !important;
}
.select2-container .select2-selection--single {
	height: 34px !important;
}
</style>
@stop


@section('content') 
   

<section class="content">

	<div class="row">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Add new cashbook</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form method="post" action="{{ route('add-cashbook.post') }}">
					{{ csrf_field() }}
					<div class="box-body">
					
						<div class="row">
							<div class="col-md-12">
								<div class="form-inline">
									<label for="voucher_account_type">Select your voucher type</label>
									<br>
									<div class="radio">
										<label>Receipt
											<input class="a_type" type="radio" name="account_type" id="account_type1" value="receipt">

										</label>
									</div>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<div class="radio">
										<label>Payment
											<input class="a_type" type="radio" name="account_type" id="account_type2" value="payment">

										</label>
									</div>
								</div>
							</div>

								<!-- <div class="content-receipt">
									<h2>hello receipt</h2>
								</div>

								<div class="content-payment">
									<h2>hello payment</h2>
								</div> -->
							<div class="col-md-6">
								<div class="form-group">
									<label for="c_vno">Select voucher no</label>
									<!-- <input type="date" name="c_vno" id="c_vno" class="form-control" placeholder="Select voucher no"> -->
									<select class="form-control select2" name="c_vno" id="c_vno">
										<option value="">-- Select your voucher no --</option>
										<?php foreach ($vouchers as $voucher): ?>
											<option value="{{ $voucher->id }}" {{ (old('c_vno') == $voucher->id) ? 'selected' : '' }}>{{ $voucher->voucher_no }}</option>
										<?php endforeach; ?>
									</select>
								</div>

								<div class="form-group">
									<label for="c_achead">Account head</label>
									<select class="form-control select2" name="c_achead" id="c_achead">
										<option value="">-- Select your account head --</option>
										<?php foreach ($acheads as $achead): ?>
											<option value="{{ $achead->id }}" {{ (old('c_achead') == $achead->id) ? 'selected' : '' }}>{{ $achead->account_head }}</option>
										<?php endforeach; ?>
									</select> 

								</div>

								
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="c_date">Select date</label>
									<input type="date" name="c_date" id="c_date" class="form-control" placeholder="Enter date">
								</div>

								<div class="form-group">
									<label for="c_amount">Amount</label>
									<input type="text" name="c_amount" id="c_amount" class="form-control" placeholder="Enter amount">
								</div>
							</div>

							<div class="col-md-12">
								<button type="submit" class="btn btn-primary btn-block">Submit</button>
							</div>

						</div>

					</div>
				</form>
			</div>
		</div>
	</div>
</section>   

@endsection


@section('scripts')
<!-- <script src="{!! asset('assets/js/js.cookie.js') !!}"></script>

<script>
	     $('.content-receipt').hide();
        $('.content-payment').hide();
       $('.a_type').on('click', function() {

            if ($('input[name="account_type"]:checked').val() == "receipt") {
                $('.content-receipt').show();
                Cookies.set('content-receipt_form', 'open');
            } else {
                $('.content-receipt').hide();
                Cookies.set('content-receipt_form', 'close');
            }
        });

        var op = Cookies.get('content-receipt_form');
         
        if ( op == 'open') {
            $('.content-receipt').show();
            $('input:radio[name="account_type"]').filter('[value="receipt"]').attr('checked', true);
            Cookies.remove('content-receipt_form');
        } else {
            $('.content-receipt').hide();
            Cookies.remove('content-receipt_form');
        }


        $('.a_type').on('click', function() {

            if ($('input[name="account_type"]:checked').val() == "payment") {
                $('.content-payment').show();
                Cookies.set('content-payment_form', 'open');
            } else {
                $('.content-payment').hide();
                Cookies.set('content-payment_form', 'close');
            }
        });

        var op = Cookies.get('content-payment_form');
         
        if ( op == 'open') {
            $('.content-payment').show();
            $('input:radio[name="account_type"]').filter('[value="payment"]').attr('checked', true);
            Cookies.remove('content-payment_form');
        } else {
            $('.content-payment').hide();
            Cookies.remove('content-payment_form');
        }

</script> -->
<script src="{!! asset('assets/plugins/select2/dist/js/select2.full.min.js') !!}"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
});
</script>
@stop
  