@extends('layouts.front')


@section('styles')
<style type="text/css" media="screen">
  .bold {
    font-weight: bold;
  }
</style>
@stop


@section('content') 
   
    <!-- Main content -->
    <section class="content">

     <h1>Assam Higher Secondary Education Council (FINMIS)</h1>

      <div class="row">
        <div class="col-lg-3 col-xs-6">
         <div class="small-box bg-aqua">
            <div class="inner">
              <h4 class="bold">Head</h4>

              <p>Wise report</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{ route('head-wise-report') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

         <div class="col-lg-3 col-xs-6">
           <div class="small-box bg-green">
            <div class="inner">
              <h4 class="bold">Day</h4>

              <p>Wise report</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{ route('day-wise-report-filter') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

       <div class="col-lg-3 col-xs-6">
           <div class="small-box bg-yellow">
            <div class="inner">
              <h4 class="bold">Receipts/Payments</h4>

              <p>Wise report</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{ route('receipts-payments-report-filter') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

       <!-- <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h4 class="bold">65</h4>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars""></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div> -->
      </div>
   
    </section>
    <!-- /.content -->

    @endsection


@section('scripts')

@stop
  