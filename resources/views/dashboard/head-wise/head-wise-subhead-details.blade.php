@extends('layouts.front')


@section('styles')
@stop


@section('content') 
   
    <!-- Main content -->
    <section class="content">

  <div class="row">
    <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Head wise report</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered" id="countit">
                <tr>
                  <th>#</th>
                  <th>Date</th>
                  <th style="width: 37%;">Particulars</th>
                  <th>Voucher no</th>
                  <th>Voucher type</th>
                  <th>Debit</th>
                  <th>Credit</th>
                </tr>

                <?php $i=1; ?>
               @foreach($voucher as $v)
                <tr>
                  <td>{{ $i }}</td>
                  <td>{{ date('d-M-Y', strtotime($v->voucher_entry_date)) }}</td>
                  <td style="width: 37%;"><a href="{{ route('subhead-details-full', $v->id) }}">{!! $v->voucher_narration !!}</a></td>
                  <td>{{ $v->voucher_no }} </td>
                  <td>{{ ucwords($v->voucher_account_type) }}</td>
                 
                  @if($v->voucher_type == 'debit')
                  <?php

                    // $amount = $v->v_amount;
                    // setlocale(LC_MONETARY, 'en_IN');
                    // $amount = money_format('%!i', $amount);
                    // echo $amount;

                  ?>
                  <td class="count-me-debit"><i class="fa fa-rupee"></i> {{ $v->voucher_amount }}.00</td>
                  @else
                  <td><i class="fa fa-rupee"></i> 0.00</td>
                  @endif

                  @if($v->voucher_type == 'credit')
                  <?php

                    // $amount = $v->v_amount;
                    // setlocale(LC_MONETARY, 'en_IN');
                    // $amount = money_format('%!i', $amount);
                    // echo $amount;

                  ?>
                  <td class="count-me-credit"><i class="fa fa-rupee"></i> {{ $v->voucher_amount }}.00</td>
                  @else
                  <td><i class="fa fa-rupee"></i> 0.00</td>
                  @endif

                </tr>
                <?php $i++; ?>
               @endforeach
             </table>

             <p class="result"></p>

             <!-- <a class="btn-btn-primary" href="{{ redirect()->back()->getTargetUrl() }}">Back</a> -->
            </div>
            <!-- /.box-body -->
            <!-- <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div> -->
          </div>
          <!-- /.box -->    
        </div>
  </div>
   
    </section>
    <!-- /.content -->

    @endsection


@section('scripts')
<script>
  $( document ).ready(function() {
    // debit part
    var resultd = [];
    var resultc = [];

      $('table tr').each(function(){
        $('.count-me-debit', this).each(function(index, val){
            if(!resultd[index]) resultd[index] = 0;
           resultd[index] += parseInt($(val).text());
        });

        $('.count-me-credit', this).each(function(index, val){
            if(!resultc[index]) resultc[index] = 0;
           resultc[index] += parseInt($(val).text());
        });

      });

      document.getElementById('countit').innerHTML += '<tr><td></td>'+'<td></td>'+'<td></td>'+'<td></td>'+'<td><strong>Total</strong></td>'+'<td><i class="fa fa-rupee"></i><strong>' + '&nbsp;' + resultd + '.00</strong></td><td><i class="fa fa-rupee"></i><strong>'+'&nbsp;'+ resultc+ '.00</strong></td></tr>';
     
  });

</script>
@stop
  