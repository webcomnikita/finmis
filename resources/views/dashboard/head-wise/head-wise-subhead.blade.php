@extends('layouts.front')


@section('styles')
@stop


@section('content') 
   
    <!-- Main content -->
    <section class="content">

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Account Head</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered" id="countit">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Sub Account head</th>
                  <th>Debit</th>
                  <th>Credit</th>
                </tr>

                <?php $i=1; ?>
               @foreach($vdetails as $vdetail)
                <tr>
                  <td>{{ $i }}</td>
                  <td><a href="{{ route('subhead-details', $vdetail->credit_id ) }}">{{ $vdetail->credit_name }}</a></td>

                  @if($vdetail->trans_type == 'debit')
                  <?php

                    // $amount = $vdetail->v_amount;
                    // setlocale(LC_MONETARY, 'en_IN');
                    // $amount = money_format('%!i', $amount);
                    // echo $amount;

                  ?>
                  <td class="count-me-debit"><i class="fa fa-rupee"></i> {{ $vdetail->v_amount }}.00</td>
                  @else
                  <td><i class="fa fa-rupee"></i> 0.00</td>
                  @endif

                  @if($vdetail->trans_type == 'credit')
                  <?php

                    // $amount = $vdetail->v_amount;
                    // setlocale(LC_MONETARY, 'en_IN');
                    // $amount = money_format('%!i', $amount);
                    // echo $amount;

                  ?>
                  <td class="count-me-credit"><i class="fa fa-rupee"></i> {{ $vdetail->v_amount }}.00</td>
                  @else
                  <td><i class="fa fa-rupee"></i> 0.00</td>
                  @endif

                </tr>
                <?php $i++; ?>
               @endforeach
             </table>

             <p class="result"></p>
            </div>
            <!-- /.box-body -->
            <!-- <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div> -->
          </div>
          <!-- /.box -->    
        </div>
  </div>
   
    </section>
    <!-- /.content -->

    @endsection


@section('scripts')
<script>
  $( document ).ready(function() {
    // debit part
    var resultd = [];
    var resultc = [];

      $('table tr').each(function(){
        $('.count-me-debit', this).each(function(index, val){
            if(!resultd[index]) resultd[index] = 0;
           resultd[index] += parseInt($(val).text());
        });

        $('.count-me-credit', this).each(function(index, val){
            if(!resultc[index]) resultc[index] = 0;
           resultc[index] += parseInt($(val).text());
        });

      });

      document.getElementById('countit').innerHTML += '<tr><td></td>'+'<td><strong>Total</strong></td>'+'<td><i class="fa fa-rupee"></i><strong>' + '&nbsp;' + resultd + '.00</strong></td><td><i class="fa fa-rupee"></i><strong>'+'&nbsp;'+ resultc+ '.00</strong></td></tr>';
     
  });
</script>
@stop
  