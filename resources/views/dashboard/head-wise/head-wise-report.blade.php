@extends('layouts.front')


@section('styles')
@stop


@section('content') 
   
    <!-- Main content -->
    <section class="content">

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Account Head</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Account head</th>
                </tr>

                <?php $i=1; ?>
                @foreach($acheads as $achead)
                <tr>
                  <td>{{ $i }}</td>
                  <td><a href="{{ route('head-wise-subhead', $achead->id) }}">{{ $achead->account_head }}</a></td>
                </tr>
                <?php $i++; ?>
                @endforeach

             </table>
            </div>
            <!-- /.box-body -->
            <!-- <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div> -->
          </div>
          <!-- /.box -->    
        </div>
  </div>
   
    </section>
    <!-- /.content -->

    @endsection


@section('scripts')

@stop
  