@extends('layouts.front')


@section('styles')
<style>
.modal.modal-wide .modal-dialog {
  width: 70%;
}
.modal-wide .modal-body {
  overflow-y: auto;
}
.bdr {
  border: 2px solid #222;
  padding: 1em;
}


.pay {
  border: 1px solid #222;
  padding: .8em;
  position: relative;
}

.signtr {
  border: 1px solid #222;
  height: 100px;
  width: 100%;

}



@media screen {
  #printSection {
      display: none;
  }
  /*#hidden-print {
    display: none;
  }

  #hide_modal {
    display: none;
  }
*/
}

@media print {
  body * {
    visibility:hidden;
    font-size: 10px;
    /*width: 100%;*/
  }
  /*#hide_modal {
    display: none;
  }

  #hidden-print {
    display: none;
  }*/

  #print-modal {
    display: none;
  }
  #printSection, #printSection * {
    visibility:visible;
  }
  #printSection {
    position:absolute;
    left:0;
    top:0;
  }
  .signtr {
  border: 1px solid #222;
  height: 50px;
  width: 20%;
 }
  .bdr {
    border: 2px solid #222;
    padding: 1em;
  }
 .pay {
    border: 1px solid #222;
    padding: .8em;
    position: relative;
    width: 20%;
  }
  .pull-right {
  float: right !important;
  }
  .pull-left {
    float: left !important;
  }
  .prnt {
    position: relative;
    width: 100%;
  }
  .width_25 {
    width: 25%;
    float: left;
  }
  .width_33 {
    width: 33.33333333%;
    float: left;
  }
  .width_16 {
    width: 16.66666667%;
    float: left;
  }

}

@page { size: auto;  margin: 0mm; }


</style>
@stop


@section('content') 
   
    <!-- Main content -->
    <section class="content">

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Accounting voucher</h3>

              <a class="pull-right btn btn-warning" id="preview" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class="fa fa-print"></i>&nbsp;Print</a>
              <!-- <a class="pull-right btn btn-warning" onclick="window.print()"><i class="fa fa-print"></i>&nbsp;Print</i></a> -->

             </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered" id="countit">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Particulars</th>
                  <th>Debit</th>
                  <th>Credit</th>
                </tr>

                <?php $i=1; ?>
               @foreach($vtrans as $vdetail)
                <tr>
                  <td>{{ $i }}</td>

                  @if($vdetail->trans_type == 'debit')
                  <td>{{ $vdetail->debit_name }}</td>
                  @else
                  <td>{{ $vdetail->credit_name }}</td>
                  @endif

                  @if($vdetail->trans_type == 'debit')
                    <td class="count-me-debit"><i class="fa fa-rupee"></i> {{ $vdetail->v_amount }}.00</td>
                  @else
                  <td><i class="fa fa-rupee"></i> 0.00</td>
                  @endif

                  @if($vdetail->trans_type == 'credit')
                    <td class="count-me-credit"><i class="fa fa-rupee"></i> {{ $vdetail->v_amount }}.00</td>
                  @else
                  <td><i class="fa fa-rupee"></i> 0.00</td>
                  @endif

                </tr>
                <?php $i++; ?>
               @endforeach
             </table>

            </div>
            <!-- /.box-body -->
           
          </div>
          <!-- /.box -->    
        </div>
  </div>
   
    </section>
    <!-- /.content -->


  <div class="modal modal-wide fade" id="confirm-print" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Accounting Voucher</h4>
        </div>

        <div id="printThis">
        <div class="modal-body">
          <a class="btn btn-warning pull-right" id="print-modal" onclick="window.print()"><i class="fa fa-print"></i>&nbsp; Print</a>

          <div class="row">
            <div class="col-md-3">
              
            </div>

            <div class="col-md-6">
              <div class="text-center">
                <h4><b>AHSEC FINMIS</b></h4>
                <span>Napaam, Sonitpur, Assam-784028</span><br>
                <span>Ph: +913712267112 , +913712273161</span><br>
                <span>email: bpathak@tezu.ernet.in</span><br>
                <span><b>{{ ucwords($vnratn) }} voucher</b></span><br>
                <span>Recurring & Salary</span><br>
              </div>
            </div>

            <div class="col-md-3">
              
            </div>

          </div>
          <div class="row">
            <div class="col-md-9">
              <span><b>Voucher no: </b> {{ $vno }} </span><br>
              <span><b>Reference no: </b> {{ $vgnrtd }}</span>
            </div>
            <div class="col-md-3">
              <span class="pull-right"><b>Dated: </b> {{ date('d-M-Y', strtotime($vdate)) }}</span>
            </div>
          </div>

          <div class="bdr">
          <table class="table table-bordered" id="countit-modal">
                <tr>
                  <th>Particulars</th>
                  <th>Debit</th>
                  <th>Credit</th>
                </tr>

               @foreach($vtrans as $vdetail)
                <tr>
                
                  @if($vdetail->trans_type == 'debit')
                  <td>{{ $vdetail->debit_name }}</td>
                  @else
                  <td>{{ $vdetail->credit_name }}</td>
                  @endif

                  @if($vdetail->trans_type == 'debit')
                  <td class="count-me-debit-modal"><i class="fa fa-rupee"></i> {{ $vdetail->v_amount }}.00</td>
                  @else
                  <td><i class="fa fa-rupee"></i> 0.00</td>
                  @endif

                  @if($vdetail->trans_type == 'credit')
                  <td class="count-me-credit-modal"><i class="fa fa-rupee"></i> {{ $vdetail->v_amount }}.00</td>
                  @else
                  <td><i class="fa fa-rupee"></i> 0.00</td>
                  @endif

                </tr>
                @endforeach
          </table>
          </div>


  <?php
   $number = $vinwords;
   $no = round($number);
   $point = round($number - $no, 2) * 100;
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array('0' => '', '1' => 'one', '2' => 'two',
    '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
    '7' => 'seven', '8' => 'eight', '9' => 'nine',
    '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
    '13' => 'thirteen', '14' => 'fourteen',
    '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
    '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
    '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
    '60' => 'sixty', '70' => 'seventy',
    '80' => 'eighty', '90' => 'ninety');
   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $number = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($number < 21) ? $words[$number] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($number / 10) * 10]
            . " " . $words[$number % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode('', $str);
  $points = ($point) ?
    "." . $words[$point / 10] . " " . 
          $words[$point = $point % 10] : '';

  // $inwords = $result . "Rupees  " . $points . " Paise";

  $inwords = $result . "Rupees Only ";
 ?> 
            <p class="mt1" id="inwords">Amount (in words) : <b>{{ ucwords($inwords) }}</b></p>
            
            <p><b>Narration: </b>{!! $vnrtn !!}</p>

           <div class="row">
            <div class="col-md-3 mt2">
              
              <div class="pay">
                Pay &nbsp;<i class="fa fa-rupee"></i> 
              </div>
             
            </div>
          </div>

          <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3">
              <div class="signtr pull-right"></div>
            </div>
          </div>

         <!--  <div class="row" id="hidden-print">
            <div class="col-md-3"><b>Jr. Accountant/ Section Officer</b></div>
            <div class="col-md-4"><b>Dy. Registrar (Finance)/ Dy.Registrar (Accounts) / Asstt.Finance Officer</b></div>
            <div class="col-md-2"><b>Finance Officer</b></div>
            <div class="col-md-3 text-center"><b>Received in full</b></div>
          </div> -->


          <div class="prnt">
            <div class="width_25"><b>Jr. Accountant/ Section Officer</b></div>
            <div class="width_33"><b>Dy. Registrar (Finance)/ Dy.Registrar (Accounts) / Asstt.Finance Officer</b></div>
            <div class="width_16"><b>Finance Officer</b></div>
            <div class="width_25 text-center"><b>Received in full</b></div>
          </div>

        </div>
      </div>
        <div class="modal-footer">
    
        </div>
      </div>
      
    </div>
  </div>


  <!--  -->

@endsection


@section('scripts')
<script type="text/javascript" src="http://www.ittutorials.in/js/demo/numtoword.js"></script>
<script>
  $( document ).ready(function() {
   
   // var inwords = $(this).attr(toWords($(this).text()));

    var resultd = [];
    var resultc = [];

    var resultdmodal = [];
    var resultcmodal = [];


      $('table tr').each(function(){
        $('.count-me-debit', this).each(function(index, val){
            if(!resultd[index]) resultd[index] = 0;
           resultd[index] += parseInt($(val).text());
        });

        $('.count-me-credit', this).each(function(index, val){
            if(!resultc[index]) resultc[index] = 0;
           resultc[index] += parseInt($(val).text());
        });

        $('.count-me-debit-modal', this).each(function(index, val){
            if(!resultdmodal[index]) resultdmodal[index] = 0;
           resultdmodal[index] += parseInt($(val).text());
        });

        $('.count-me-credit-modal', this).each(function(index, val){
            if(!resultcmodal[index]) resultcmodal[index] = 0;
           resultcmodal[index] += parseInt($(val).text());
        });

      });

      document.getElementById('countit').innerHTML += '<tr><td></td>'+'<td><strong>Total</strong></td>'+'<td><i class="fa fa-rupee"></i><strong>' + '&nbsp;' + resultd + '.00</strong></td><td><i class="fa fa-rupee"></i><strong>'+'&nbsp;'+ resultc+ '.00</strong></td></tr>';

      document.getElementById('countit-modal').innerHTML += '<tr>'+'<td><strong>Total</strong></td>'+'<td id="resultdmodal"><i class="fa fa-rupee"></i><strong>' + '&nbsp;' + resultdmodal + '.00</strong></td><td id="resultcmodal"><i class="fa fa-rupee"></i><strong>'+'&nbsp;'+ resultcmodal + '.00</strong></td></tr>';


      document.getElementById('inwords').innerHTML += '<div id="word"></div>';
   





  });



  $('#preview').on('click', function() { 

    $("#confirm-print").modal({
        backdrop: 'static',
        keyboard: false
    });

  });

  document.getElementById("print-modal").onclick = function () {
    printElement(document.getElementById("printThis"));
}

function printElement(elem) {
    var domClone = elem.cloneNode(true);
    
    var $printSection = document.getElementById("printSection");
    
    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }
    
    $printSection.innerHTML = "";
    $printSection.appendChild(domClone);
    window.print();
}


 


</script>
@stop
  