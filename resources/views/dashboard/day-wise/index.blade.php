@extends('layouts.front')


@section('styles')
@stop


@section('content') 
   
<!-- Main content -->
<section class="content">

<div class="row">
	<div class="col-md-10 col-md-offset-1">
          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Select date range</h3>
            </div>
            <!-- /.box-header -->
           
            <form action="{{ route('day-wise-report.post') }}" method="post">
            	{{ csrf_field() }}
            	<div class="box-body">
            		<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
	            					<label>Select date from</label>
									<input type="date" class="form-control" id="from" name="from_date">
								</div>

								<div class="col-md-6">
	            					<label>Select date to</label>
									<input type="date" class="form-control" id="to" name="to_date">
								</div>

	            				<div class="col-md-12 mt1">
		            				<button type="submit" class="btn btn-warning btn-block">Search</button>
		            			</div>
	            			</div>
						</div>
            			

            		</div>
            	</div>
            </form>
				
            
        </div>
    </div>
</div>

</section>
<!-- /.content -->

@endsection


@section('scripts')
<script>

$( document ).ready(function() {

// function getLastWeek(){
//     var today = new Date();
//     var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
//     return lastWeek ;
// }

// var lastWeek = getLastWeek();
// var lastWeekMonth = lastWeek.getMonth() + 1;
// var lastWeekDay = lastWeek.getDate();
// var lastWeekYear = lastWeek.getFullYear();

// var lastWeekDisplay = lastWeekMonth + "/" + lastWeekDay + "/" + lastWeekYear;
// var lastWeekDisplayPadded = ("00" + lastWeekMonth.toString()).slice(-2)+ "/" + ("00" + lastWeekDay .toString()).slice(-2)+ "/" + ("0000" + lastWeekYear .toString()).slice(-4);

// alert(lastWeekDisplay);
// alert(lastWeekDisplayPadded);

// var date = new Date();
// var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
// var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

// alert(lastDay);


});

</script>
@stop
  