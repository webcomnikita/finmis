@extends('layouts.front')


@section('styles')
<style>
  .br1 {
    border-right: 3px solid #222;
  }
</style>
@stop


@section('content') 
   
<!-- Main content -->
<section class="content">

 <div class="row">
    <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Receipt / Payment wise report</h3>

              <button type="button" class="btn btn-warning pull-right" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-search"></i>&nbsp; Period</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            	<div class="row">
            		<div class="col-md-6">
             
              <table class="table table-bordered" id="count-receipt">
              	<h3 class="bold">Receipt</h3>
                <tr>
                 
                  <th>Particulars</th>
                  <th>Amount</th>
                </tr>

                @foreach($filterreceipt as $v)
                <tr>
                
                
                  @if($v->trans_type == 'debit')
                  <td>{{ $v->debit_name }}</td>
                  @elseif($v->trans_type == 'credit')
                  <td>{{ $v->credit_name }}</td>
                  @endif
                 
                  <td class="count-me-debit">{{ $v->v_amount }}.00</td>
                       

                </tr>
			    @endforeach
		    </table>
			</div>

			<div class="col-md-6">
				<table class="table table-bordered" id="count-payment">
					<h3 class="bold">Payment</h3>
                <tr>
                 
                  <th>Particulars</th>
                  <th>Amount</th>
                 
                </tr>

			     @foreach($filterpayment as $vp)
                <tr>
                
                  @if($vp->trans_type == 'debit')
                  <td>{{ $vp->debit_name }}</td>
                  @elseif($vp->trans_type == 'credit')
                  <td>{{ $vp->credit_name }}</td>
                  @endif
                 
                  <td class="count-me-credit">{{ $vp->v_amount }}.00</td>
                 

				  <td></td>
				  <td></td>
                  

                </tr>
			    @endforeach
             </table>
			</div>
			</div>
             <p class="result"></p>
             

             <!-- <a class="btn-btn-primary" href="{{ redirect()->back()->getTargetUrl() }}">Back</a> -->
          
            </div>
            <!-- /.box-body -->
           
          </div>
          <!-- /.box -->    
        </div>
  </div>

</section>
<!-- /.content -->


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Search date wise</h4>
        </div>
        <div class="modal-body">
          <div class="row">

			<form action="{{ route('receipts-payments-report.post') }}" method="post">
				{{ csrf_field() }}
	          	<div class="col-md-6">
	          		<label>From</label>
	          		<input type="date" name="from-date" class="form-control">
	          	</div>

	          	<div class="col-md-6">
	          		<label>To</label>
	          		<input type="date" name="to-date" class="form-control">
	          	</div>

	          	<div class="col-md-12 mt1">
	          		<button type="submit" class="btn btn-warning btn-block">Search</button>
	          	</div>
			</form>
          </div>
        </div>
       
      </div>
      
    </div>
  </div>

@endsection


@section('scripts')
<script>
  $( document ).ready(function() {
    // debit part
    var resultd = [];
    var resultc = [];

      $('table tr').each(function(){
        $('.count-me-debit', this).each(function(index, val){
            if(!resultd[index]) resultd[index] = 0;
           resultd[index] += parseInt($(val).text());
        });

        $('.count-me-credit', this).each(function(index, val){
            if(!resultc[index]) resultc[index] = 0;
           resultc[index] += parseInt($(val).text());
        });

      });

      document.getElementById('count-receipt').innerHTML += '<tr><td><strong>Total</strong></td>'+'<td><i class="fa fa-rupee"></i><strong>' + '&nbsp;' + resultd + '.00</strong></td></tr>';

      document.getElementById('count-payment').innerHTML += '<tr><td><strong>Total</strong></td>'+'<td><i class="fa fa-rupee"></i><strong>'+'&nbsp;'+ resultc+ '.00</strong></td></tr>';
     
  });

</script>
@stop
  
  