@extends('layouts.front')


@section('styles')
<!-- <link rel="stylesheet" href="{!! asset('assets/plugins/datepicker/bootstrap-datepicker.min.css') !!}"> -->
 <!-- #874bbc; -->
<style>
 .btn-pink {
  background-color: rgba(135, 75, 188, 0.9);
  color: #fff;
 }
 .btn-pink:hover {
  background-color: rgba(135, 75, 188, 1);
  color: #fff;
 }
</style>
@stop


@section('content') 

<!-- Main content -->
<section class="content">

  <form method="post" action="{{ route('add-voucher.post') }}" onsubmit="return validateForm()">
          {{ csrf_field() }}

  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Add new voucher</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <!-- <form method="post" action="{{ route('add-voucher.post') }}">
          {{ csrf_field() }} -->
          <div class="box-body">

          <?php 
//$utrno = str_random(8);
//echo $utrno;
//die();  FINMIS/2018-19/F/84

$icode = str_random(5);
$cdate = date("Y");
$nxtyr = date('y', strtotime('+1 year'));
$utrno = 'FINMIS'.'/'.$cdate.'-'.$nxtyr.'/'.'F'.'/'.$icode;
//echo $utrno;



// echo date('y', strtotime('+1 year'));


//die();
            ?>

            <div class="form-group">
              <label for="voucher_no">Voucher no</label>
              <input type="text" class="form-control" name="voucher_no" id="voucher_no" placeholder="Enter voucher no" required="required">
            </div>

            <div class="form-group mt2">
              <label for="voucher_date">Voucher Date</label>
              <input type="date" class="form-control datepicker" name="voucher_date" id="voucher_date" data-date-format="Y-m-d" placeholder="Enter voucher date" required="required">
            </div>


            <!-- <div class="form-group">
              <label for="voucher_amount">Voucher Amount</label>
              <input type="text" class="form-control" name="voucher_amount" id="voucher_amount" placeholder="Enter voucher amount" required="required">
            </div> -->


            <div class="form-inline">
                <label for="voucher_account_type">Select your voucher type</label>
                <br>
                <div class="radio">
                  <label>Receipt
                    <input type="radio" class="vtype" name="voucher_account_type" id="voucher_account_type1" value="receipt">

                  </label>
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div class="radio">
                  <label>Payment
                    <input type="radio" class="vtype" name="voucher_account_type" id="voucher_account_type2" value="payment">

                  </label>
                </div>
              </div>


            <div id="amount-paid">
              <div class="form-group">
                <label for="voucher_narration">Voucher Narration</label>
                <textarea class="form-control voucher_narration" name="voucher_narration" id="voucher_narrationp">Being amount paid to  </textarea>
              </div>
            </div>

            <div id="amount-receipt">
              <div class="form-group">
                <label for="voucher_narration">Voucher Narration</label>
                <textarea class="form-control voucher_narration" name="voucher_narration" id="voucher_narrationr">Being amount receipt from  </textarea>
              </div>
           </div>

  
            <div class="form-inline">
              <label for="voucher_mode">Select your voucher mode</label>
              <br>
              <div class="radio">
                <label>Cash
                  <input class="vmode" type="radio" name="voucher_mode" id="voucher_mode1" value="cash">

                </label>
              </div>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <div class="radio">
                <label>Demand draft
                  <input class="vmode" type="radio" name="voucher_mode" id="voucher_mode2" value="dd">

                </label>
              </div>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <div class="radio">
                <label>Challan
                  <input class="vmode" type="radio" name="voucher_mode" id="voucher_mode3" value="challan">

                </label>
              </div>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <div class="radio">
                <label>Online payment
                  <input class="vmode" type="radio" name="voucher_mode" id="voucher_mode4" value="onlinepay">

                </label>
              </div>
            </div>


            <div class="content-cash">
              <p>Select your bank for cash purpose</p>
            </div>

            <div class="content-dd">
              <!-- DD select mode -->
              <div class="form-group">
                <label for="dd_no">Demand draft no</label>
                <input type="text" class="form-control" name="dd_no" id="dd_no" placeholder="Enter demand draft no">
              </div>

              <div class="form-group">
                <label for="dd_date">Demand draft date</label>
                <input type="date" class="form-control datepicker" name="dd_date" data-date-format="Y-m-d" id="dd_date" placeholder="Enter demand draft date">
              </div>
              <!-- end of dd select mode -->
            </div>


            <div class="content-challan">
              <!-- Challan select mode -->

              <div class="form-group">
                <label for="challan_no">Challan no</label>
                <input type="text" class="form-control" name="challan_no" id="challan_no" placeholder="Enter challan no">
              </div>

              <div class="form-group">
                <label for="challan_date">Challan date</label>
                <input type="date" class="form-control datepicker" name="challan_date" data-date-format="Y-m-d" id="challan_date" placeholder="Enter challan date">
              </div>
              <!-- end of challan select mode -->
            </div>

            <?php 
//$utrno = str_random(8);
//echo $utrno;
//die();

//$icode = str_random(8);
//$utrno = 'FINMIS'.$icode;
//echo $utrno;
//die();
            ?>

            <div class="content-onlinepay">
              <div class="form-group">
                <label for="utrno">Your UTR no</label>
                <input type="text" class="form-control" name="utrno" id="utrno" placeholder="Enter utr no">
              </div>
              <!-- <p>Your UTR no is auto generated, please view your voucher details for the utr no.</p> -->
            </div>

            <div class="form-group">
              <div class="@if ($errors->has('bank_id')) has-error @endif">
                <label for="bank_id">Select bank</label>
                <select class="form-control" name="bank_id" id="bank_id" required="required">
                  <option value="">-- Select your bank --</option>
                  <?php foreach ($bank as $bankmaster): ?>
                    <option value="{{ $bankmaster->id }}" {{ (old('bank_id') == $bankmaster->id) ? 'selected' : '' }}>{{ $bankmaster->bankname }}</option>
                  <?php endforeach; ?>
                </select>
                @if ($errors->has('bank_id'))
                <p class="help-block">{{ $errors->first('bank_id') }}</p>
                @endif
              </div>
            </div>



            <!-- <div class="amount-paid">  -->

              <!--  <label>Select receipt\payment account</label> -->

           

              <div id="final_result">
              <div>Your total debit <span class="red"></span> balance is : <span id="result" class="red"> </span> </div>

              <div>Your total credit <span class="red"></span> balance is : <span id="result1" class="red"> </span> </div>


              <span class="red" id="tot_result"></span>
              </div>

              <div id="debits" class="mt2">
                <div class="debit">
                  <div class="row">
                    <div class="col-md-2">
                      <select name="voucher_type[]" id="voucher_type" class="voucher_type form-control" required="required">
                        <option value="">select account type</option>
                        <option value="debit">Debit</option>
                        <option value="credit">Credit</option>
                      </select>
                    </div>

                    <div class="col-md-4">
                      <select class="form-control account_head_id" name="account_head_id[]" id="account_head_id" required="required">
                        <option value="">-- Select your account head --</option>
                        <?php foreach ($acheads as $achead): ?>
                          <option value="{{ $achead->id }}" data-themeid="{{ $achead->id }}" {{ (old('account_head_id') == $achead->id) ? 'selected' : '' }}>{{ $achead->account_head }}</option>
                        <?php endforeach; ?>
                      </select>   
                    </div>

                    <div class="col-md-4">
<!-- <select name="sub_head_id[]" id="sub_head_id" class="form-control">
<option value=""></option>
</select> -->

<select name="sub_head_id[]" id="sub_head_id" class="form-control sub_head_id" required="required">
  <option value="">-- Select your sub-account head --</option>
  <?php foreach ($subacheads as $subachead): ?>
    <option value="{{ $subachead->id }}" {{ (old('sub_head') == $subachead->id) ? 'selected' : '' }}>{{ $subachead->sub_head }}</option>
  <?php endforeach; ?>
</select>

</div> 

<div class="col-md-2">
  <input type="text" name="debit[]" id="debit" placeholder="Enter balance" class="form-control balance" required="required" >
</div>
</div>
</div>
</div>

<br>
<span class="red font-sm">* </span>
<button type="button" id="add_debit" class="btn btn-warning">Add new row</button>
<button type="button" id="final" class="btn btn-success" onclick="WriteCookie();">Add balance to compare</button>
<a data-toggle="modal" id="preview" class="btn btn-pink" data-backdrop="static" data-keyboard="false">Preview</a>
<br>

</div>
<!-- /.box-body -->

<div class="box-footer">
  <!-- <button type="submit" class="btn btn-primary btn-block dcbtn">Submit</button> -->
</div>
<!-- </form> -->
</div>
<!-- /.box -->
</div>
</div>




<div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Confirm Submit
            </div>
            <div class="modal-body">
                <!-- Are you sure you want to submit the following details? -->

                <!-- We display the details entered by the user here -->
                <table class="table">
                    <tr>
                        <th>Voucher no</th>
                        <td id="vno"></td>
                    </tr>
                    <tr>
                        <th>Voucher date</th>
                        <td id="vdate"></td>
                    </tr>
                    <tr>
                        <th>Voucher type</th>
                        <td id="vactype"></td>
                    </tr>
                    <tr>
                        <th>Voucher narration</th>
                        <td id="vnarration"></td>
                    </tr>
                    <tr>
                        <th>Voucher mode</th>
                        <td id="vmode"></td>
                    </tr>
                    <tr>
                        <th>Bank</th>
                        <td id="vbank"></td>
                    </tr>
                    <tr>
                        <th>Demand draft no</th>
                        <td id="ddno"></td>
                    </tr>
                    <tr>
                        <th>Demand draft date</th>
                        <td id="dddate"></td>
                    </tr>
                    <tr>
                        <th>Challan no</th>
                        <td id="cno"></td>
                    </tr>
                    <tr>
                        <th>Challan date</th>
                        <td id="cdate"></td>
                    </tr>
                    <tr>
                        <th>UTR no</th>
                        <td id="utrno"></td>
                    </tr>
                    <tr>
                      <th>Account type</th>
                      <td id="atype"></td>
                    </tr>
                    <tr>  
                      <th>Account head</th>
                      <td id="vachd"></td>
                    </tr>
                    <tr>
                      <th>Sub account head</th>
                      <td id="vsbachd"></td>
                    </tr>
                    <tr>
                      <th>Balance</th>
                      <td id="vbal"></td>
                    </tr>


                </table>

            </div>

  <div class="modal-footer">
    
            <button type="button" class="btn btn-default" data-dismiss="modal">Reset</button>
            <button type="submit" class="btn btn-primary dcbtn">Submit</button>
          
        </div>
    </div>
</div>
</form>
</section>
<!-- /.content -->


@endsection


@section('scripts')

<script type="text/javascript">

  $(document).ready(function() {

    $('#preview').hide();
    // $('#rset').hide(); 

    $('#amount-paid').hide();
    $('#amount-receipt').hide();

    $('input[name="voucher_account_type"]').on('click', function() {
      if ($(this).val() == 'receipt') {
        $('#amount-receipt').show();
      }
      else {
        $('#amount-receipt').hide();
      }

      if ($(this).val() == 'payment') {
        $('#amount-paid').show();
      }
      else {
        $('#amount-paid').hide();
      }
    });


    $('.content-cash').hide();
    $('.content-dd').hide();
    $('.content-challan').hide();
    $('.content-onlinepay').hide();

    $('input[name="voucher_mode"]').on('click', function() {
      if ($(this).val() == 'cash') {
        $('.content-cash').show();
      }
      else {
        $('.content-cash').hide();
      }

      if ($(this).val() == 'dd') {
        $('.content-dd').show();
      }
      else {
        $('.content-dd').hide();
      }

      if ($(this).val() == 'challan') {
        $('.content-challan').show();
      }
      else {
        $('.content-challan').hide();
      }

      if ($(this).val() == 'onlinepay') {
        $('.content-onlinepay').show();
      }
      else {
        $('.content-onlinepay').hide();
      }
    });

  });

  function validateForm()
    {

        var a=document.forms["Form"]["voucher_no"].value;
        var b=document.forms["Form"]["voucher_date"].value;
        var c=document.forms["Form"]["voucher_account_type"].value;
        var d=document.forms["Form"]["voucher_narration"].value;
        var e=document.getElementById("voucher_mode");

        var f=document.forms["Form"]["dd_no"].value;
        var g=document.forms["Form"]["dd_date"].value;

        var h=document.forms["Form"]["challan_no"].value;
        var x=document.forms["Form"]["challan_date"].value;

        var y=document.forms["Form"]["utrno"].value;

        var k=document.forms["Form"]["bank_id"].value;
          if(a==null || a=="",b==null || b=="",c==null || c=="",d==null || d=="", e.value==null || e.value=="")
        {
            alert("Please Fill All Required Field");
            return false;
        }

        if(e && e.value=='dd'){
          if(f==null || f=="",g==null || g==""){
            alert("Please Fill Demand Draft Details");
            return false;
          }
        }

        if(e && e.value=='challan'){
          if(h==null || h=="",x==null || x==""){
            alert("Please Fill Challan Details");
            return false;
          }
        }

        if(e && e.value=='onlinepay'){
          if(y==null || y==""){
            alert("Please Fill UTR no");
            return false;
          }
        }


    }



  function WriteCookie()
{
  $('.remove').hide(); 

  if ($("input[name='debit[]']").val() == 0) {
    alert("Enter some value!");
        return;
  }

  var str = $("input[name='debit[]']")
  .map(function ()
  {
    return $(this).val();

  }).get();

var total_debit = 0;
var total_credit = 0;
var vtype = [];
var temo_value="";
$.each($(".voucher_type option:selected"), function(){ 

  vtype.push($(this).val());

  var my_total_debit=0; 

  for (var i=0; i<str.length; i++){
    if(vtype[i]=='debit'){
      my_total_debit += + str[i];
    }else{
      console.log("vtype[i] "+i+"=" +vtype[i])  ; 
    }


    document.getElementById("result").innerHTML = my_total_debit; 
  } 


  var my_total_credit=0;

  for (var i=0; i<str.length; i++){
    if(vtype[i]=='credit'){
      my_total_credit += + str[i];
    }else{
      console.log("vtype[i] "+i+"=" +vtype[i]) ; 
    }

    document.getElementById("result1").innerHTML = my_total_credit;


  } 

 
 if((my_total_debit == my_total_credit) )
  {
    $('#add_debit').hide();
    $('#final').hide();
      $('#preview').show(); 
      // $('#rset').show(); 

  }else{
    alert("Enter all field values");
    $('#preview').hide();
  }

  // if (my_total_debit == my_total_credit) 
  // {
  //   $('#add_debit').hide();
  //   $('#final').hide();
  //     $('#preview').show(); 
  //     // $('#rset').show(); 

  // }else{
  //   $('#preview').hide();
  // }

});
}


$('#preview').on('click', function() { 

  $("#confirm-submit").modal({
      backdrop: 'static',
      keyboard: false
  });

 $('#vno').text($('#voucher_no').val());
 $('#vdate').text($('#voucher_date').val());
 $('#vactype').text($("input[name='voucher_account_type']:checked").val());

 $('#vnarration').text($('.voucher_narration').val());
 $('#vmode').text($("input[name='voucher_mode']:checked").val());
 
 var sel = document.getElementById('bank_id');
 var opt = sel.options[sel.selectedIndex];
 var vbnk = opt.text ;
 $('#vbank').text(vbnk);

 $('#ddno').text($('#dd_no').val());
 $('#dddate').text($('#dd_date').val());
 $('#cno').text($('#challan_no').val());
 $('#cdate').text($('#challan_date').val());
 $('#utrno').text($('#utrno').val());

var vtype1 = [];
$.each($(".voucher_type option:selected"), function(){ 
  vtype1.push($(this).val());
});
$('#atype').text(vtype1);

var vachd = [];
$.each($(".account_head_id option:selected"), function(){ 
  vachd.push($(this).text());
});
$('#vachd').text(vachd);

var vsbachd = [];
$.each($(".sub_head_id option:selected"), function(){ 
  vsbachd.push($(this).text());
});
$('#vsbachd').text(vsbachd);

var vbal = [];
$.each($(".balance option:selected"), function(){ 
  vbal.push($(this).val());
});
$('#vbal').text(vbal);
 

  
});




$('#add_debit').on('click', function() { 

  if ($("input[name='debit[]']").val() == 0) {
    alert("Enter some value!");
        return;
  }

  $('#debits').append('<div id="debits" class="mt2">' + '<div class="debit">' + '<div class="row">' + '<div class="col-md-1">' + '<select name="voucher_type[]" id="voucher_type" class="voucher_type form-control" required="required">' + '<option value="">select account type</option>' + '<option value="debit">Debit</option>' +'<option value="credit">Credit</option>' + '</select>' + '</div>' + '<div class="col-md-4">' + '<select class="form-control account_head_id" name="account_head_id[]" id="account_head_id" required="required">' + '<option value="">Select receipt/payment account</option>' + '<?php foreach ($acheads as $achead): ?>' + '<option value="{{ $achead->id }}" data-themeid="{{ $achead->id }}" {{ (old('account_head_id') == $achead->id) ? 'selected' : '' }}>{{ $achead->account_head }}</option>' + '<?php endforeach; ?>' + '</select>' + '</div>' + '<div class="col-md-4">' + '<select name="sub_head_id[]" id="sub_head_id" class="form-control sub_head_id" required="required">' + '<option value="">Select your sub-account head </option>' + '<?php foreach ($subacheads as $subachead): ?>' + '<option value="{{ $subachead->id }}" {{ (old('sub_head') == $subachead->id) ? 'selected' : '' }}>{{ $subachead->sub_head }}</option>' + '<?php endforeach; ?>' + '</select>' + '</div>' + '<div class="col-md-2">' + '<input type="text" name="debit[]" id="debit" placeholder="Enter balance" class="form-control balance" required="required">' + '</div>' + '<div class="col-md-1">' + '<button class="btn btn-danger remove">X</button>' + '</div>' + '</div>' + '</div>' + '</div>')

// var str = $("input[name='debit[]']")
// .map(function ()
// {
//   return $(this).val();

// }).get();

// var $block;
// var total_debit = 0;
// var total_credit = 0;
// var vtype = [];
// var temo_value="";
// $.each($(".voucher_type option:selected"), function(){ 

//   vtype.push($(this).val());

//   var my_total_debit=0; 

//   for (var i=0; i<str.length; i++){
//     if(vtype[i]=='debit'){
//       my_total_debit += + str[i];
//     }else{
//       console.log("vtype[i] "+i+"=" +vtype[i])  ; 
//     }


//     document.getElementById("result").innerHTML = my_total_debit; 

//   } 


//   var my_total_credit=0;

//   for (var i=0; i<str.length; i++){
//     if(vtype[i]=='credit'){
//       my_total_credit += + str[i];
//     }else{
//       console.log("vtype[i] "+i+"=" +vtype[i])  ; 
//     }

//     document.getElementById("result1").innerHTML = my_total_credit;

//   } 

//   if (my_total_debit == my_total_credit) 
//   {
//     // $('#debits').hide();
//     // $('#add_debit').hide();
//     $('.dcbtn').show();   
//   }else{
//     $('.dcbtn').hide();
//   }

  
// });


//document.getElementById("result").innerHTML = total_debit;

//var vtype_result = (vtype.join(",")) ;
//document.getElementById("vtrype").innerHTML = vtype_result; 


return false; //prevent form submission

// var total_balance = total_debit += + total_credit;
// document.getElementById("tot_result").innerHTML = total_balance;

});

// $('#final_result').hide();

// $('#comp_balnc').on('click', function(){

//     $('#final_result').show();

// });


$('#debits').on('click', '.remove', function() {
  $(this).parents('.debit').remove();
return false; //prevent form submission
});

</script>
@stop
