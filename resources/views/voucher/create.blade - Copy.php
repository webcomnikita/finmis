@extends('layouts.front')


@section('styles')
<!-- <link rel="stylesheet" href="{!! asset('assets/plugins/datepicker/bootstrap-datepicker.min.css') !!}"> -->

@stop


@section('content') 

<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Add new voucher</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form method="post" action="{{ route('add-voucher.post') }}">
          {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="voucher_no">Voucher no</label>
              <input type="text" class="form-control" name="voucher_no" id="voucher_no" placeholder="Enter voucher no" required="required">
            </div>

            <div class="form-group mt2">
              <label for="voucher_date">Voucher Date</label>
              <input type="date" class="form-control datepicker" name="voucher_date" id="voucher_date" data-date-format="Y-m-d" placeholder="Enter voucher date" required="required">
            </div>


            <!-- <div class="form-group">
              <label for="voucher_amount">Voucher Amount</label>
              <input type="text" class="form-control" name="voucher_amount" id="voucher_amount" placeholder="Enter voucher amount" required="required">
            </div> -->


            <div class="form-inline">
                <label for="voucher_account_type">Select your voucher type</label>
                <br>
                <div class="radio">
                  <label>Receipt
                    <input type="radio" name="voucher_account_type" id="voucher_account_type1" value="receipt">

                  </label>
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div class="radio">
                  <label>Payment
                    <input type="radio" name="voucher_account_type" id="voucher_account_type2" value="payment">

                  </label>
                </div>
              </div>


            <div id="amount-paid">
            <div class="form-group">
              <label for="voucher_narration">Voucher Narration</label>
              <textarea class="form-control" name="voucher_narration" id="voucher_narration">Being amount paid to  </textarea>
            </div>
            </div>

            <div id="amount-receipt">
            <div class="form-group">
              <label for="voucher_narration">Voucher Narration</label>
              <textarea class="form-control" name="voucher_narration" id="voucher_narration">Being amount receipt from  </textarea>
            </div>
          </div>


            <div class="form-inline">
              <label for="voucher_mode">Select your voucher mode</label>
              <br>
              <div class="radio">
                <label>Cash
                  <input class="vmode" type="radio" name="voucher_mode" id="voucher_mode1" value="cash">

                </label>
              </div>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <div class="radio">
                <label>Demand draft
                  <input class="vmode" type="radio" name="voucher_mode" id="voucher_mode2" value="dd">

                </label>
              </div>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <div class="radio">
                <label>Challan
                  <input class="vmode" type="radio" name="voucher_mode" id="voucher_mode3" value="challan">

                </label>
              </div>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <div class="radio">
                <label>Online payment
                  <input class="vmode" type="radio" name="voucher_mode" id="voucher_mode4" value="onlinepay">

                </label>
              </div>
            </div>


            <div class="content-cash">
              <p>Select your bank for cash purpose</p>
            </div>

            <div class="content-dd">
              <!-- DD select mode -->
              <div class="form-group">
                <label for="dd_no">Demand draft no</label>
                <input type="text" class="form-control" name="dd_no" id="dd_no" placeholder="Enter demand draft no">
              </div>

              <div class="form-group">
                <label for="dd_date">Demand draft date</label>
                <input type="date" class="form-control datepicker" name="dd_date" data-date-format="Y-m-d" id="dd_date" placeholder="Enter demand draft date">
              </div>
              <!-- end of dd select mode -->
            </div>


            <div class="content-challan">
              <!-- Challan select mode -->

              <div class="form-group">
                <label for="challan_no">Challan no</label>
                <input type="text" class="form-control" name="challan_no" id="challan_no" placeholder="Enter challan no">
              </div>

              <div class="form-group">
                <label for="challan_date">Challan date</label>
                <input type="date" class="form-control datepicker" name="challan_date" data-date-format="Y-m-d" id="challan_date" placeholder="Enter challan date">
              </div>
              <!-- end of challan select mode -->
            </div>

            <?php 
//$utrno = str_random(8);
//echo $utrno;
//die();  FINMIS/2018-19/F/84

$icode = str_random(8);
$utrno = 'FINMIS'.$icode;
echo $utrno;
die();
            ?>

            <div class="content-onlinepay">
              <div class="form-group">
                <label for="utrno">Your UTR no</label>
                <input type="text" class="form-control" name="utrno" id="utrno" placeholder="Enter utr no">
              </div>
              <!-- <p>Your UTR no is auto generated, please view your voucher details for the utr no.</p> -->
            </div>

            <div class="form-group">
              <div class="@if ($errors->has('bank_id')) has-error @endif">
                <label for="bank_id">Select bank</label>
                <select class="form-control" name="bank_id" id="bank_id" required="required">
                  <option value="">-- Select your bank --</option>
                  <?php foreach ($bank as $bankmaster): ?>
                    <option value="{{ $bankmaster->id }}" {{ (old('bank_id') == $bankmaster->id) ? 'selected' : '' }}>{{ $bankmaster->bankname }}</option>
                  <?php endforeach; ?>
                </select>
                @if ($errors->has('bank_id'))
                <p class="help-block">{{ $errors->first('bank_id') }}</p>
                @endif
              </div>
            </div>



            <!-- <div class="amount-paid">  -->

              <!--  <label>Select receipt\payment account</label> -->

           

              <div id="final_result">
              <div>Your total debit <span class="red"></span> balance is : <span id="result" class="red"> </span> </div>

              <div>Your total credit <span class="red"></span> balance is : <span id="result1" class="red"> </span> </div>


              <span class="red" id="tot_result"></span>
              </div>

              <div id="debits" class="mt2">
                <div class="debit">
                  <div class="row">
                    <div class="col-md-2">
                      <select name="voucher_type[]" id="voucher_type" class="voucher_type form-control" required="required">
                        <!-- <option value="">select account type</option> -->
                        <option value="debit">Debit</option>
                        <option value="credit">Credit</option>
                      </select>
                    </div>

                    <div class="col-md-4">
                      <select class="form-control" name="account_head_id[]" id="account_head_id" required="required">
                        <option value="">-- Select your account head --</option>
                        <?php foreach ($acheads as $achead): ?>
                          <option value="{{ $achead->id }}" data-themeid="{{ $achead->id }}" {{ (old('account_head_id') == $achead->id) ? 'selected' : '' }}>{{ $achead->account_head }}</option>
                        <?php endforeach; ?>
                      </select>   
                    </div>

                    <div class="col-md-4">
<!-- <select name="sub_head_id[]" id="sub_head_id" class="form-control">
<option value=""></option>
</select> -->

<select name="sub_head_id[]" id="sub_head_id" class="form-control" required="required">
  <option value="">-- Select your sub-account head --</option>
  <?php foreach ($subacheads as $subachead): ?>
    <option value="{{ $subachead->id }}" {{ (old('sub_head') == $subachead->id) ? 'selected' : '' }}>{{ $subachead->sub_head }}</option>
  <?php endforeach; ?>
</select>

</div> 

<div class="col-md-2">
  <input type="text" name="debit[]" id="debit" placeholder="Enter balance" class="form-control" required="required" >
</div>
</div>
</div>
</div>

<br>
<span class="red font-sm">* </span>
<button id="add_debit" class="btn btn-warning">Add new row</button>
<!-- <button id="comp_balnc" class="btn btn-success">Add balance to compare</button> -->
<br>

</div>
<!-- /.box-body -->

<div class="box-footer">
  <button type="submit" class="btn btn-primary btn-block dcbtn">Submit</button>
</div>
</form>
</div>
<!-- /.box -->
</div>
</div>

</section>
<!-- /.content -->

@endsection


@section('scripts')

<script type="text/javascript">

  $(document).ready(function() {

    $('.dcbtn').hide();

    $('#amount-paid').hide();
    $('#amount-receipt').hide();

    $('input[name="voucher_account_type"]').on('click', function() {
      if ($(this).val() == 'receipt') {
        $('#amount-receipt').show();
      }
      else {
        $('#amount-receipt').hide();
      }

      if ($(this).val() == 'payment') {
        $('#amount-paid').show();
      }
      else {
        $('#amount-paid').hide();
      }
    });


    $('.content-cash').hide();
    $('.content-dd').hide();
    $('.content-challan').hide();
    $('.content-onlinepay').hide();

    $('input[name="voucher_mode"]').on('click', function() {
      if ($(this).val() == 'cash') {
        $('.content-cash').show();
      }
      else {
        $('.content-cash').hide();
      }

      if ($(this).val() == 'dd') {
        $('.content-dd').show();
      }
      else {
        $('.content-dd').hide();
      }

      if ($(this).val() == 'challan') {
        $('.content-challan').show();
      }
      else {
        $('.content-challan').hide();
      }

      if ($(this).val() == 'onlinepay') {
        $('.content-onlinepay').show();
      }
      else {
        $('.content-onlinepay').hide();
      }
    });

  });

//Clone and Remove Form Fields

$('#add_debit').on('click', function() { 
// $('#debits').append('<div class="debit"><input type="text" name="debit[]"><button class="remove">x</button></div>');


  $('#debits').append('<div id="debits" class="mt2">' + '<div class="debit">' + '<div class="row">' + '<div class="col-md-1">' + '<select name="voucher_type[]" id="voucher_type" class="voucher_type form-control" required="required">' + '<option value="debit">Debit</option>' +'<option value="credit">Credit</option>' + '</select>' + '</div>' + '<div class="col-md-4">' + '<select class="form-control" name="account_head_id[]" id="account_head_id" required="required">' + '<option value="">Select receipt/payment account</option>' + '<?php foreach ($acheads as $achead): ?>' + '<option value="{{ $achead->id }}" data-themeid="{{ $achead->id }}" {{ (old('account_head_id') == $achead->id) ? 'selected' : '' }}>{{ $achead->account_head }}</option>' + '<?php endforeach; ?>' + '</select>' + '</div>' + '<div class="col-md-4">' + '<select name="sub_head_id[]" id="sub_head_id" class="form-control" required="required">' + '<option value="">Select your sub-account head </option>' + '<?php foreach ($subacheads as $subachead): ?>' + '<option value="{{ $subachead->id }}" {{ (old('sub_head') == $subachead->id) ? 'selected' : '' }}>{{ $subachead->sub_head }}</option>' + '<?php endforeach; ?>' + '</select>' + '</div>' + '<div class="col-md-2">' + '<input type="text" name="debit[]" id="debit" placeholder="Enter balance" class="form-control" required="required">' + '</div>' + '<div class="col-md-1">' + '<button class="btn btn-danger remove">X</button>' + '</div>' + '</div>' + '</div>' + '</div>')

var str = $("input[name='debit[]']")
.map(function ()
{
  return $(this).val();

}).get();

var $block;
var total_debit = 0;
var total_credit = 0;
var vtype = [];
var temo_value="";
$.each($(".voucher_type option:selected"), function(){ 

  vtype.push($(this).val());

  var my_total_debit=0; 

  for (var i=0; i<str.length; i++){
    if(vtype[i]=='debit'){
      my_total_debit += + str[i];
    }else{
      console.log("vtype[i] "+i+"=" +vtype[i])  ; 
    }


    document.getElementById("result").innerHTML = my_total_debit; 

  } 


  var my_total_credit=0;

  for (var i=0; i<str.length; i++){
    if(vtype[i]=='credit'){
      my_total_credit += + str[i];
    }else{
      console.log("vtype[i] "+i+"=" +vtype[i])  ; 
    }

    document.getElementById("result1").innerHTML = my_total_credit;

  } 

  if (my_total_debit == my_total_credit) 
  {
    // $('#debits').hide();
    // $('#add_debit').hide();
    $('.dcbtn').show();   
  }else{
    $('.dcbtn').hide();
  }

  
});


//document.getElementById("result").innerHTML = total_debit;

//var vtype_result = (vtype.join(",")) ;
//document.getElementById("vtrype").innerHTML = vtype_result; 


return false; //prevent form submission

// var total_balance = total_debit += + total_credit;
// document.getElementById("tot_result").innerHTML = total_balance;

});

// $('#final_result').hide();

// $('#comp_balnc').on('click', function(){

//     $('#final_result').show();

// });


$('#debits').on('click', '.remove', function() {
  $(this).parents('.debit').remove();
return false; //prevent form submission
});

</script>
@stop
