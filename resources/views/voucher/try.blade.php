<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>cookei test</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <style type="text/css" media="screen">
  	.mt2 {
  		margin-top: 2em;
  	}
  </style>
</head>
<body>
  

<!-- <form name="myform" action="">
	 Enter name: <input type="text" name="customer"/>
	 <input type="button" value="Set Cookie" onclick="WriteCookie();"/>
</form> -->

<div class="container">

	<div class="row">
		<div id="coocie-result">
			<table class="table table-bordered">
				<thead>
			      <tr>
			        <th>#</th>
			        <th> Debit Total </th>
			        <th> Credit Total </th>
			      </tr>
			    </thead>

			    <tbody>
			      <tr>
			        <td>1</td>
			        <td id="result"></td>
			        <td id="result1"></td>
			      </tr>
			  </tbody>

			</table>
		</div>
	</div>


	<div id="debits" class="mt2">
		<div class="debit">
			<div class="row">

				<div class="col-md-2">
					<select name="voucher_type[]" id="voucher_type" class="voucher_type form-control" required="required">
						<!-- <option value="">select account type</option> -->
						<option value="debit">Debit</option>
						<option value="credit">Credit</option>
					</select>
				</div>

				<div class="col-md-4">
					<select class="form-control" name="account_head_id[]" id="account_head_id" required="required">
						<option value="">-- Select your account head --</option>
						<?php foreach ($acheads as $achead): ?>
							<option value="{{ $achead->id }}" data-themeid="{{ $achead->id }}" {{ (old('account_head_id') == $achead->id) ? 'selected' : '' }}>{{ $achead->account_head }}</option>
						<?php endforeach; ?>
					</select>  
				</div>


				<div class="col-md-4">
					<select name="sub_head_id[]" id="sub_head_id" class="form-control" required="required">
						<option value="">-- Select your sub-account head --</option>
						<?php foreach ($subacheads as $subachead): ?>
							<option value="{{ $subachead->id }}" {{ (old('sub_head') == $subachead->id) ? 'selected' : '' }}>{{ $subachead->sub_head }}</option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="col-md-2">
					<input type="text" name="debit[]" id="debit" placeholder="Enter balance" class="form-control" required="required" >
				</div>

			</div>
		</div>
	</div>


<div class="row">
	<div class="col-md-12">
			<button id="add_debit" class="btn btn-warning">Add new row</button>
			<button id="final" class="btn btn-success" onclick="WriteCookie();">Add balance</button>
			<br>


			<button type="submit" class="btn btn-primary btn-block dcbtn">Submit</button>
		</div>
</div>
</div>
  
 

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type = "text/javascript">
 
    // function WriteCookie()
    // {
    //    if( document.myform.customer.value == "" ){
    //       alert("Enter some value!");
    //       return;
    //    }
    //    cookievalue= escape(document.myform.customer.value) + ";";
    //    document.cookie="name=" + cookievalue;
    //    alert("Setting Cookies : " + "name=" + cookievalue );
    // }
 
$(document).ready(function() {
    $('.dcbtn').hide();
});

function WriteCookie()
{
	if ($("input[name='debit[]']").val() == 0) {
		alert("Enter some value!");
        return;
	}

	var str = $("input[name='debit[]']")
	.map(function ()
	{
	  return $(this).val();

	}).get();

var total_debit = 0;
var total_credit = 0;
var vtype = [];
var temo_value="";
$.each($(".voucher_type option:selected"), function(){ 

  vtype.push($(this).val());

  var my_total_debit=0; 

  for (var i=0; i<str.length; i++){
    if(vtype[i]=='debit'){
      my_total_debit += + str[i];
    }else{
      console.log("vtype[i] "+i+"=" +vtype[i])  ; 
    }


    document.getElementById("result").innerHTML = my_total_debit; 
   // alert(my_total_debit);

  } 


  var my_total_credit=0;

  for (var i=0; i<str.length; i++){
    if(vtype[i]=='credit'){
      my_total_credit += + str[i];
    }else{
      console.log("vtype[i] "+i+"=" +vtype[i])  ; 
    }

    document.getElementById("result1").innerHTML = my_total_credit;

  } 

  if (my_total_debit == my_total_credit) 
  {
  	$('#add_debit').hide();
     $('.dcbtn').show();   
  }else{
    $('.dcbtn').hide();
  }



//alert(my_total_debit);
});
}

$('#add_debit').on('click', function() { 


  $.cookie("rented_car", $('#debits').append('<div id="debits" class="mt2">' + '<div class="debit">' + '<div class="row">' + '<div class="col-md-1">' + '<select name="voucher_type[]" id="voucher_type" class="voucher_type form-control" required="required">' + '<option value="debit">Debit</option>' +'<option value="credit">Credit</option>' + '</select>' + '</div>' + '<div class="col-md-4">' + '<select class="form-control" name="account_head_id[]" id="account_head_id" required="required">' + '<option value="">Select receipt/payment account</option>' + '<?php foreach ($acheads as $achead): ?>' + '<option value="{{ $achead->id }}" data-themeid="{{ $achead->id }}" {{ (old('account_head_id') == $achead->id) ? 'selected' : '' }}>{{ $achead->account_head }}</option>' + '<?php endforeach; ?>' + '</select>' + '</div>' + '<div class="col-md-4">' + '<select name="sub_head_id[]" id="sub_head_id" class="form-control" required="required">' + '<option value="">Select your sub-account head </option>' + '<?php foreach ($subacheads as $subachead): ?>' + '<option value="{{ $subachead->id }}" {{ (old('sub_head') == $subachead->id) ? 'selected' : '' }}>{{ $subachead->sub_head }}</option>' + '<?php endforeach; ?>' + '</select>' + '</div>' + '<div class="col-md-2">' + '<input type="text" name="debit[]" id="debit" placeholder="Enter balance" class="form-control" required="required">' + '</div>' + '<div class="col-md-1">' + '<button class="btn btn-danger remove">X</button>' + '</div>' + '</div>' + '</div>' + '</div>'))

// var str = $("input[name='debit[]']")
// .map(function ()
// {
//   return $(this).val();

// }).get();

// var $block;
// var total_debit = 0;
// var total_credit = 0;
// var vtype = [];
// var temo_value="";
// $.each($(".voucher_type option:selected"), function(){ 

//   vtype.push($(this).val());

//   var my_total_debit=0; 

//   for (var i=0; i<str.length; i++){
//     if(vtype[i]=='debit'){
//       my_total_debit += + str[i];
//     }else{
//       console.log("vtype[i] "+i+"=" +vtype[i])  ; 
//     }


//     document.getElementById("result").innerHTML = my_total_debit; 

//   } 


//   var my_total_credit=0;

//   for (var i=0; i<str.length; i++){
//     if(vtype[i]=='credit'){
//       my_total_credit += + str[i];
//     }else{
//       console.log("vtype[i] "+i+"=" +vtype[i])  ; 
//     }

//     document.getElementById("result1").innerHTML = my_total_credit;

//   } 

//   if (my_total_debit == my_total_credit) 
//   {
//      $('.dcbtn').show();   
//   }else{
//     $('.dcbtn').hide();
//   }

  
// });

return false; 

});


$('#debits').on('click', '.remove', function() {
  $(this).parents('.debit').remove();
return false; //prevent form submission
});


</script>
</body>
</html>