@extends('layouts.front')


@section('styles')
 <link rel="stylesheet" href="{!!asset('assets/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}">
@stop


@section('content') 
   
    <!-- Main content -->
    <section class="content container-fluid">

     <div class="box">
            <div class="box-header">
              <h3 class="box-title">View all voucher details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="v-details" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Voucher no</th>
                  <th>Voucher type</th>
                  <th>Voucher date</th>
                  <th>Voucher amount</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
            	<?php $i=1; ?>
                <?php foreach ($vouchers as $voucher): ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td>{{ $voucher->generated_voucher_no }}</td>
                  <td>{{ ucwords($voucher->voucher_account_type) }}</td>
                  <td>{{ date('Y-M-d', strtotime($voucher->voucher_date)) }}</td>
                  <td>{{ $voucher->voucher_amount }}</td>
                  <td><a href="{{ route('view-voucher-single', $voucher->id) }}" class="btn btn-success" title="View details">
                      <i class="fa fa-eye"></i></a></td>
                </tr>
                <?php $i++; ?>
                <?php endforeach; ?>
                </tbody>
                <!-- <tfoot>
                <tr>
                  <th>#</th>
                  <th>Voucher no</th>
                  <th>Voucher type</th>
                  <th>Voucher date</th>
                  <th>Voucher amount</th>
                  <th>Action</th>
                </tr>
                </tfoot> -->
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->

    @endsection


@section('scripts')
<script src="{!!asset ('assets/plugins/datatables.net/js/jquery.dataTables.min.js') !!}"></script>
<script src="{!!asset ('assets/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}"></script>
<script>
  $(function () {
    $('#v-details').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })
  })
</script>
@stop
  