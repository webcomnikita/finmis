@extends('layouts.front')


@section('styles')
<style>
.modal.modal-wide .modal-dialog {
  width: 70%;
}
.modal-wide .modal-body {
  overflow-y: auto;
}
.bdr {
  border: 2px solid #222;
  padding: 1em;
}


.pay {
  border: 1px solid #222;
  padding: .8em;
  position: relative;
}

.signtr {
  border: 1px solid #222;
  height: 100px;
  width: 100%;

}



@media screen {
  #printSection {
      display: none;
  }
  /*#hidden-print {
    display: none;
  }

  #hide_modal {
    display: none;
  }
*/
}

@media print {
  body * {
    visibility:hidden;
    font-size: 10px;
    /*width: 100%;*/
  }
  /*#hide_modal {
    display: none;
  }

  #hidden-print {
    display: none;
  }*/

  #print-modal {
    display: none;
  }
  #printSection, #printSection * {
    visibility:visible;
  }
  #printSection {
    position:absolute;
    left:0;
    top:0;
  }
  .signtr {
  border: 1px solid #222;
  height: 50px;
  width: 20%;
 }
  .bdr {
    border: 2px solid #222;
    padding: 1em;
  }
 .pay {
    border: 1px solid #222;
    padding: .8em;
    position: relative;
    width: 20%;
  }
  .pull-right {
  float: right !important;
  }
  .pull-left {
    float: left !important;
  }
  .prnt {
    position: relative;
    width: 100%;
  }
  .width_25 {
    width: 25%;
    float: left;
  }
  .width_33 {
    width: 33.33333333%;
    float: left;
  }
  .width_16 {
    width: 16.66666667%;
    float: left;
  }

}

@page { size: auto;  margin: 0mm; }


</style>
@stop


@section('content') 
   
    <!-- Main content -->
    <section class="content container-fluid">

     <div class="box">
            <div class="box-header">
              	<h3 class="box-title">Voucher details for voucher no <span class="label label-warning">{{ $voucher->voucher_no }}</span></h3>

              	<button class="btn btn-danger pull-right printpage" data-target="#myModal" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class="fa fa-print"></i> Print</button>
	  		</div>
            <!-- /.box-header -->
            <div class="box-body">
            	<div class="row">
					<div class="col-md-6">
						<div class="well">
			               <!-- <strong>Voucher type : </strong>
			               <span>{{ $voucher->voucher_type }}</span><br> -->

						   <strong>Voucher date : </strong>
			               <span>{{ date('Y-M-d', strtotime($voucher->voucher_date)) }}</span><br>

			               <strong>Voucher mode : </strong>
			               <span>{{ $voucher->voucher_mode }}</span><br>

			               <strong>Bank name : </strong>
			               <span>{{ $voucher->bank_id }}</span><br>

			            </div>
					</div>

					<div class="col-md-6">
						<div class="well">
						   <strong>Voucher amount : </strong>
			               <span>{{ $voucher->voucher_amount }}</span><br>

			               <strong>Voucher entry date : </strong>
			               <span>{{ date('Y-M-d', strtotime($voucher->created_at)) }}</span><br>

			               <strong>Voucher account head : </strong>
			               <span>{{ $voucher->account_head_id }}</span><br>

			               <strong>Voucher sub-account head : </strong>
			               <span>{{ $voucher->sub_head_id }}</span><br>

			           </div>
					</div>


					@if($voucher->voucher_mode == 'dd')
					<div class="col-md-6">
						<div class="well">
							<strong>Demand draft no : </strong>
			                <span>{{ $voucher->dd_no }}</span><br>

			                <strong>Demand draft date : </strong>
			                <span>{{ date('Y-M-d', strtotime($voucher->dd_date)) }}</span><br>

			                <strong>Voucher account type : </strong>
			               <span>{{ ucwords($voucher->voucher_account_type) }}</span><br>
			             </div>
					</div>
					@endif

					@if($voucher->voucher_mode == 'cash')
					<div class="col-md-6">
						<div class="well">
							<strong>Voucher mode : </strong>
			                <span>Your voucher mode is cash</span><br>

			                <strong>Voucher account type : </strong>
			               <span>{{ ucwords($voucher->voucher_account_type) }}</span><br>
					    </div>
					</div>
					@endif

					@if($voucher->voucher_mode == 'challan')
					<div class="col-md-6">
						<div class="well">
							<strong>Challan no : </strong>
			                <span>{{ $voucher->challan_no }}</span><br>

			                <strong>Challan date : </strong>
			                <span>{{ date('Y-M-d', strtotime($voucher->challan_date)) }}</span><br>

			                <strong>Voucher account type : </strong>
			               <span>{{ ucwords($voucher->voucher_account_type) }}</span><br>
			             </div>
					</div>
					@endif

					@if($voucher->voucher_mode == 'onlinepay')
					<div class="col-md-6">
						<div class="well">
							<strong>UTR no : </strong>
			                <span>{{ $voucher->utrno }}</span><br>

			                <strong>Voucher account type : </strong>
			               <span>{{ ucwords($voucher->voucher_account_type) }}</span><br>

			             </div>
					</div>
					@endif

					<div class="col-md-6">
						<div class="well">
							<strong>Voucher Narration : </strong>
			                <span>{!! $voucher->voucher_narration !!}</span><br>
		           		</div>
					</div>
               </div>

               <div class="row">
               	<div class="col-md-12">
               		<a class="btn btn-success btn-block" href="{{ route('voucher.edit',$voucher->id )}}">
               			<i class="fa fa-pencil"></i>
               			Edit
               		</a>
               	</div>
               </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->



     <!-- Modal -->
  <div class="modal modal-wide fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Print voucher record</h4>
        </div>

        <div id="printThis">
        <div class="modal-body">
        	<a class="btn btn-warning pull-right" id="print-modal" onclick="window.print()"><i class="fa fa-print"></i>&nbsp; Print</a>
			
			<div class="row">
				<div class="col-md-12">

					<h2 class="text-center">Voucher details for voucher no {{ $voucher->generated_voucher_no }}</h2>
				   <!-- <strong>Voucher type : </strong>
	               <span>{{ $voucher->voucher_type }}</span><br> -->

				   <strong>Voucher date : </strong>
	               <span>{{ date('Y-M-d', strtotime($voucher->voucher_date)) }}</span><br>

	               <strong>Voucher mode : </strong>
	               <span>{{ $voucher->voucher_mode }}</span><br>

	               <strong>Bank name : </strong>
	               <span>{{ $bname }}</span><br>



					<strong>Voucher amount : </strong>
	               <span>{{ $voucher->voucher_amount }}</span><br>

	               <strong>Voucher entry date : </strong>
	               <span>{{ date('Y-M-d', strtotime($voucher->created_at)) }}</span><br>

	               <strong>Voucher account head : </strong>
	               <span>{{ $acheadname }}</span><br>

	               <strong>Voucher sub-account head : </strong>
	               <span>{{ $sacheadname }}</span><br>

	               @if($voucher->voucher_mode == 'dd')
					<strong>Demand draft no : </strong>
		            <span>{{ $voucher->dd_no }}</span><br>

		            <strong>Demand draft date : </strong>
		            <span>{{ date('Y-M-d', strtotime($voucher->dd_date)) }}</span><br>

		            <strong>Voucher account type : </strong>
		            <span>{{ ucwords($voucher->voucher_account_type) }}</span><br>
			     	@endif

					@if($voucher->voucher_mode == 'cash')
					<strong>Voucher mode : </strong>
			        <span>Your voucher mode is cash</span><br>

			        <strong>Voucher account type : </strong>
			        <span>{{ ucwords($voucher->voucher_account_type) }}</span><br>
					@endif

					@if($voucher->voucher_mode == 'challan')
					<strong>Challan no : </strong>
	                <span>{{ $voucher->challan_no }}</span><br>

	                <strong>Challan date : </strong>
	                <span>{{ date('Y-M-d', strtotime($voucher->challan_date)) }}</span><br>

	                <strong>Voucher account type : </strong>
	               <span>{{ ucwords($voucher->voucher_account_type) }}</span><br>
			    	@endif

					@if($voucher->voucher_mode == 'onlinepay')
					<strong>UTR no : </strong>
	                <span>{{ $voucher->utrno }}</span><br>

	                <strong>Voucher account type : </strong>
	                <span>{{ ucwords($voucher->voucher_account_type) }}</span><br>
					@endif

					<strong>Voucher Narration : </strong>
	                <span>{!! $voucher->voucher_narration !!}</span><br>
           		
				</div>
			</div>

        </div>
    </div>
</div>
</div>
</div>
<!-- end of modal -->

    @endsection


@section('scripts')
<script>
	document.getElementById("print-modal").onclick = function () {
    printElement(document.getElementById("printThis"));
}

function printElement(elem) {
    var domClone = elem.cloneNode(true);
    
    var $printSection = document.getElementById("printSection");
    
    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }
    
    $printSection.innerHTML = "";
    $printSection.appendChild(domClone);
    window.print();
}
</script>
@stop
  